﻿namespace BusinessLogic
{
    public interface IEmberAdat
    {
        string cimke { get; set; }
        IGene[] ellentetesNemiKromoszoma { get; set; }
        IGene[] gének { get; set; }
        bool nem { get; set; }
        IGene[] nemikromoszoma { get; set; }
    }
}