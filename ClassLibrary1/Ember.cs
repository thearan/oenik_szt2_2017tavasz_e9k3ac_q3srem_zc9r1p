﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Ember
    {

        IEmberAdat Adat;
        private static int[] nemiKromoszomaLokuszok = new int[] { 2, 3, 5, 7, 8, 9, 13, 80, 82, 83, 84, 86, 97, 111 };
        #region Csak klaszterezésnél használjuk őket:
        public int _HozzamVanLegkozelebb { get; set; }
        public List<int> _HozzajukLegkozelebbEnVagyok { get; set; }
        public object _Klaszter { get; set; } 
        #endregion


        public bool _Nem
        {
            get { return Adat.nem; }
            private set { Adat.nem = value; }
        }
        public IGene[] _Gének
        {
            get { return Adat.gének; }
            private set { Adat.gének = value; }
        }
        public IGene[] _Nemikromoszoma
        {
            get
            {
                return Adat.nemikromoszoma;
            }

            private set
            {
                Adat.nemikromoszoma = value;
            }
        }
        public IGene[] _EllentetesNemiKromoszoma
        {
            get
            {
                return Adat.ellentetesNemiKromoszoma;
            }

            private set
            {
                Adat.ellentetesNemiKromoszoma = value;
            }
        }
        public string _VegsoOs
        {
            get { return Adat.cimke.Substring(Adat.cimke.LastIndexOf(@"\") + 1); }
            set { Adat.cimke = value; }
        }
        public double _FormaiTorzsag
        {
            get { return genetikai_tavolsag4zet(this._Gének, null, 0, 50); }

        }
        public double _SzinbeliTorzsag
        {
            get { return genetikai_tavolsag4zet(this._Gének, null, 80, 130); }
        }
        public double _AsszimetrikusTorzsag
        {
            get { return genetikai_tavolsag4zet(this._Gének, null, 50, 80); }

        }


        public Ember(IEmberAdat initAdat)
        {
            Adat = initAdat;
            #region nem init

            string s = _VegsoOs.Substring(0, 1);
            if (s == "F" || s == "f") { _Nem = true; }
            else if (s == "N" || s == "n") { _Nem = false; }
            else
            { throw new IsmeretlenAzEgyedNemeException(); }

            #endregion
            #region Kromoszómák init
            this._Nemikromoszoma = new IGene[] { _Gének[2], _Gének[3], _Gének[5], _Gének[7], _Gének[8], _Gének[9], _Gének[13], _Gének[80], _Gének[82], _Gének[83], _Gének[84], _Gének[86], _Gének[97], _Gének[111] };
            this._EllentetesNemiKromoszoma = IGeneFactory.Create_Gene(new ushort[] { 32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768 }, _VegsoOs);
            #endregion
            for (int i = 0; i < _Gének.Length; i++)
            {
                _Gének[i]._Cimke = _VegsoOs;
            }
            _HozzajukLegkozelebbEnVagyok = new List<int>();

        }
        private Ember(Ember N, Ember F)
        {
            bool tempnem = Rnd._Next();
            Ember A; Ember E; if (tempnem == false)
            { A = N; E = F; }
            else { A = F; E = N; }
            #region vegsoos init
            string tempvegsoos = A._VegsoOs;
            #endregion
            #region Gének init
            IGene[] tempgenek = new IGene[A._Gének.Length];
            for (int i = 0; i < A._Gének.Length; i++)
            {
                IGene temptempgene = Rnd._VeletlenValaszt(N._Gének[i], F._Gének[i]);
                tempgenek[i] = IGeneFactory.Create_Gene(temptempgene._TheNumber, temptempgene._Cimke);
            }

            for (int i0 = 0; i0 < nemiKromoszomaLokuszok.Length; i0++)
            {
                int i = nemiKromoszomaLokuszok[i0];

                IGene tempGene = A._Gének[i];
                tempgenek[i] = IGeneFactory.Create_Gene(tempGene._TheNumber, tempGene._Cimke);
            }
            #endregion

            this.Adat = IEmberAdatFactory.Create_EmberAdat(tempgenek, tempvegsoos, false);
            this._Nem = tempnem;
            //this._VegsoOs = tempvegsoos;
            //this._Gének = tempgenek;
            this.mutasys();
            #region Kromoszómák init
            this._Nemikromoszoma = new IGene[] { _Gének[2], _Gének[3], _Gének[5], _Gének[7], _Gének[8], _Gének[9], _Gének[13], _Gének[80], _Gének[82], _Gének[83], _Gének[84], _Gének[86], _Gének[97], _Gének[111] };
            this._EllentetesNemiKromoszoma = new IGene[] { E._Gének[2], E._Gének[3], E._Gének[5], E._Gének[7], E._Gének[8], E._Gének[9], E._Gének[13], E._Gének[80], E._Gének[82], E._Gének[83], E._Gének[84], E._Gének[86], E._Gének[97], E._Gének[111] };
            #endregion
            _HozzajukLegkozelebbEnVagyok = new List<int>();


        }
        private static int atlag_gyerekszam(double tav)
        {
            double termekenyseg = SimulationParameters._Termekenyseg;
            int n = 0;
            if (SimulationParameters._Belterjessegi_szelekcio_mertek < Math.Sqrt(tav))
            {
                if ((double)((int)termekenyseg) == termekenyseg) n = (int)termekenyseg;
                else
                {
                    bool temp = Rnd._Random_Valoszinuseg((termekenyseg - (double)((int)termekenyseg)) * (double)100);
                    if (temp) { n = (int)termekenyseg + 1; }
                    else { n = (int)termekenyseg; }
                }
            }
            return n;
        }
        public void _Genesys(Ember egyed, ref List<Ember> ujgeneracio_listajanak_referenciaja)
        {
            if (this._Nem != false || egyed._Nem != true)
            { /*inkompatibilis nemek*/ return; }

            int gyerekszám = atlag_gyerekszam(this._Genetikai_Tavolsag4zet1(egyed));//egy "pár" gyerekszáma a kettejük közti gen. távolságtól függjön


            for (int i = 0; i < gyerekszám; ++i)
            {
                Ember temp = new Ember(this, egyed);
                if (temp._FormaiTorzsag < SimulationParameters._TorzShape_Szelekcio * 360000000)
                {
                    if (temp._SzinbeliTorzsag < SimulationParameters._TorzColor_Szelekcio * 225000000)
                    {
                        if (temp._AsszimetrikusTorzsag < SimulationParameters._TorzAsszim_Szelekcio * 25000000)
                        {
                            ujgeneracio_listajanak_referenciaja.Add(temp);
                        }
                        else { /*Megsemmisítés oka: asszimetria */}
                    }
                    else { /*Megsemmisítés oka:torz színek */}
                }
                else { /*Megsemmisítés oka:torz forma*/}
            }

        }
        private void mutasys()
        {
            for (int i = 0; i < SimulationParameters._Mutacios_Rata; i++)
            {
                if (Rnd._Random_Valoszinuseg(SimulationParameters._Mutacios_Rata * 100))
                {
                    int idx = Rnd._Next(0, _Gének.Length);
                    int pluszminusz = Rnd._Next(-SimulationParameters._Mutacio_Erosseg, SimulationParameters._Mutacio_Erosseg + 1);

                    int temp = (int)(_Gének[idx]._TheNumber) + (pluszminusz);
                    if (temp > (int)ushort.MaxValue) { temp = ushort.MaxValue; }
                    else if (temp < (int)ushort.MinValue) { temp = ushort.MinValue; }
                    else { _Gének[idx]._TheNumber = (ushort)temp; }
                }
            }
        }
        private double genetikai_tavolsag4zet(IGene[] gen, IGene[] gen0, int tol, int ig)
        {
            double sum = 0;
            if (gen0 == null)
            {
                for (int i = tol; i < ig; ++i)
                {
                    double temp = 32768 - gen[i]._TheNumber;
                    sum += temp * temp;
                }
            }
            else
            {
                for (int i = tol; i < ig; ++i)
                {
                    double temp = gen0[i]._TheNumber - gen[i]._TheNumber;
                    sum += temp * temp;
                }
            }
            return sum;
        }
        /// <summary>
        /// Genetikai távolságszámítás. Gender-specifikus paraméterek esetében keresztösszehasonlítással.
        /// </summary>
        /// <param name="másikEgyed">a másik egyen</param>
        /// <returns>Nem a kettő közti távolságot, hanem annak négyzetét adja vissza </returns>
        public double _Genetikai_Tavolsag4zet1(Ember másikEgyed)
        {

            if (this._Nem != másikEgyed._Nem)
            {
                return (genetikai_tavolsag4zet(this._Gének, másikEgyed._Gének, 0, 130) -
                        genetikai_tavolsag4zet(this._Nemikromoszoma, másikEgyed._Nemikromoszoma, 0, 14) +
                        genetikai_tavolsag4zet(this._EllentetesNemiKromoszoma, másikEgyed._Nemikromoszoma, 0, 14) +
                        genetikai_tavolsag4zet(this._Nemikromoszoma, másikEgyed._EllentetesNemiKromoszoma, 0, 14));
            }
            return (genetikai_tavolsag4zet(this._Gének, másikEgyed._Gének, 0, 130) +
                    genetikai_tavolsag4zet(this._EllentetesNemiKromoszoma, másikEgyed._EllentetesNemiKromoszoma, 0, 14));
        }

        /// <summary>
        /// Genetikai távolságszámítás gender-specifikus paraméterek teljes kihagyásával.
        /// </summary>
        /// <param name="másikEgyed">A másik egyed</param>
        /// <returns>Nem a genetikai távolságot, hanem annak négyzetét adja vissza</returns>
        public double _Genetikai_Tavolsag4zet2(Ember másikEgyed)
        {
            return (genetikai_tavolsag4zet(this._Gének, másikEgyed._Gének, 0, 130) -
                    genetikai_tavolsag4zet(this._Nemikromoszoma, másikEgyed._Nemikromoszoma, 0, 14));

        }

        public override string ToString()
        {
            return String.Format("{0} leszarmazott", _VegsoOs);
        }
    }

    public class EmberAdat : IEmberAdat
    {
        public bool nem { get; set; } //false a nő;
        public IGene[] gének { get; set; }
        public IGene[] nemikromoszoma { get; set;}
        public IGene[] ellentetesNemiKromoszoma { get; set; }
        public string cimke {get; set;}

        public EmberAdat(IGene[] gének, string cimke, bool tartalmazhateujcimket = true)
        {

            this.gének = gének;
            this.cimke = cimke;
            if (tartalmazhateujcimket)
            {
                DAL_Kezelo_Singleton._Add_Cimke(cimke.Substring(cimke.LastIndexOf(@"\") + 1));
            }
        }
        public EmberAdat()
        {

        }
    }

    class IsmeretlenAzEgyedNemeException : Exception
    {}



}

