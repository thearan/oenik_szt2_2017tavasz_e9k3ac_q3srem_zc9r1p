﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Kolonia
    {
        public List<Ember> _Egyedek {get; set;}
        public int _Lakossag  {get {return _Egyedek.Count;}}

        public Kolonia()
        {
            _Egyedek = new List<Ember>();
        }
        public void _Bepakol(List<Ember> egyedek)
        {
            if (_Egyedek.Count == 0)
            {
                _Egyedek = (List<Ember>)egyedek;
            }
            else
            {
                _Egyedek = _Egyedek.Union(egyedek).ToList();
            }
        }
        public void _Genesys()
        {
            List<Ember> temp = Rnd._RandomCsere(_Egyedek);
            List<Ember> ujgen = new List<Ember>();
            int i = 0;
            int j = 0;
            while (i < temp.Count && j < temp.Count)
            {
               
                while (i < temp.Count && temp[i]._Nem)
                {
                    ++i;
                }
                while (j < temp.Count && !temp[j]._Nem)
                {
                    ++j;
                }
                if (i < temp.Count && j < temp.Count)
                {
                    temp[i]._Genesys(temp[j], ref ujgen);
                    ++i;
                    ++j;
                }
            }

            this._Egyedek = ujgen;
            
        }
        public void _Genocyd_lokal(int maxlak, ref int RefglobalLak)
        {
            Ember[] temp = _Egyedek.ToArray();
            if (this._Lakossag > maxlak)
            { 
                for (int i = 0; i < temp.Length; i++)
                {
                    if (Rnd._Random_Valoszinuseg(SimulationParameters._JarvanyterjedesiHalalozasiArany))
                    {
                        _Egyedek.Remove(temp[i]);
                        RefglobalLak--;
                    }
                }                
            }
        }

        public Kolonia X_Felsoszomszed { get; set; }
        public Kolonia X_Jobbszomszed { get; set; }
        public Kolonia X_Alsoszomszed { get; set; }
        public Kolonia X_Balszomszed { get; set; }


        public override string ToString()
        {
            return String.Format("Telepules; Lakosság:{0}", _Lakossag);
        }
    }
}
