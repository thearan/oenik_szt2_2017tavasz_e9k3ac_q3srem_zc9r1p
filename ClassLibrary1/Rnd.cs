﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Rnd
    {
        public static void _Initialisys()
        { r = new Random(); }
        static Random r = new Random();

        public static int _Next(int a, int b)
        {
            return r.Next(a, b);
        }
        public static bool _Next()
        {
            if (r.Next(0, 2) == 1)
            { return true; }
            else { return false; }
        }
        public static IGene _VeletlenValaszt(IGene a, IGene b)
        {
            if (_Next() == false)
            {
                return a;
            }
            else
            {
                return b;
            }
        }
        public static List<T> _RandomCsere<T>(List<T> O)
        {
            Visszateveses_Randomizator rndlotto = new Visszateveses_Randomizator(0, O.Count - 1);
            List<T> O2 = new List<T>(O.Count);
            for (int i = 0; i < O.Count; i++)
            {
                O2.Add(O[rndlotto._Kivesz()]);
            }

            return O2;
        }
        public static bool _Random_Valoszinuseg(double mekkoraValoszinuseggelLegyenIgaz_szazalek)
        {
            if (mekkoraValoszinuseggelLegyenIgaz_szazalek >= 100) { return true; }
            if (mekkoraValoszinuseggelLegyenIgaz_szazalek <= 0) { return false; }

            int temp = Rnd._Next(0, 101);

            return temp < mekkoraValoszinuseggelLegyenIgaz_szazalek;
        }


        class Visszateveses_Randomizator
        {
            List<int> Lista;

            public Visszateveses_Randomizator(int a, int b) //mind2 benne van; a<b
            {
                if (b < a) { int c = a; a = b; b = c; }

                Lista = new List<int>(b - a + 1);
                for (int i = 0; i < b - a + 1; i++)
                {
                    Lista.Add(i);
                }
            }
            public int _Kivesz()
            {
                int tempIdx = Rnd._Next(0, Lista.Count);
                int temp = Lista[tempIdx];
                Lista.RemoveAt(tempIdx);
                return temp;
            }
        }
   
    }
}
