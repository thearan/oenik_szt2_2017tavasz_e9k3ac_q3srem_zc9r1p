﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DataAccessLayer;

namespace BusinessLogic
{


    public class IO_Ember
    {
        static byte[] fejlec;
        public IO_Ember()
        {
            if (File.Exists("atlag0.fg")) _Read("atlag0.fg");
        }

        public List<IEmberAdat> _Read(string FileVagyMappanev, bool torol_e = false)
        {
            List<IEmberAdat> emberadatok = new List<IEmberAdat>();

            if (Directory.Exists(FileVagyMappanev))
            {
                string[] fileokcimei = Directory.GetFiles(FileVagyMappanev);
                for (int i = 0; i < fileokcimei.Length; ++i)
                {
                    if (fileokcimei[i].Substring(fileokcimei[i].Length - 3, 3) != ".fg") { continue; }

                    emberadatok.Add(IEmberAdatFactory.Create_EmberAdat(konvert(File.ReadAllBytes(fileokcimei[i])), fileokcimei[i].Substring(0, fileokcimei[i].Length - 3)));

                    if (torol_e) File.Delete(fileokcimei[i]);
                }
            }
            else
            {
                if (FileVagyMappanev.Substring(FileVagyMappanev.Length - 3, 3) == ".fg")
                {
                    emberadatok.Add(
                        IEmberAdatFactory.Create_EmberAdat(
                            konvert(File.ReadAllBytes(FileVagyMappanev)),
                            FileVagyMappanev.Substring(0, FileVagyMappanev.Length - 3)));
                }
            }
            return emberadatok;
        }

        /// <summary>
        /// Kiírja az egyedet fájlba
        /// </summary>
        /// <param name="u">kiírandó egyed génjei</param>
        /// <param name="név">fájlnév .fg kiterjesztés nélkül</param>
        public void _Write(IGene[] u, string név)
        {
            byte[] b = rekonvert(u);
            if (File.Exists(név + ".fg"))
            {
                int i = 2;
                while (File.Exists(név + i + ".fg"))
                {
                    ++i;
                }
                File.WriteAllBytes(név + i + ".fg", b);
            }
            else
            {
                File.WriteAllBytes(név + ".fg", b);
            }
            
        }
        public void _Write(List<Ember> egyedek, string név)
        {
            foreach (Ember u0 in egyedek)
            {
                _Write(u0._Gének, név);
            }
        }
        IGene[] konvert(byte[] s)
        {
            if (fejlec == null)
            {
                fejlec = new byte[40];
                for (int i = 0; i < 40; i++)
                {
                    fejlec[i] = s[i];
                }
            }
            IGene[] gének = new IGene[130];
            int i0 = 0;
            for (int i = 41; i < s.Length; i = i + 2)
            {
                ushort lsb = (ushort)(s[i - 1]);
                ushort msb0 = (ushort)(s[i] + (byte)128);
                ushort msb = (ushort)((ushort)256 * msb0);
                gének[i0] = IGeneFactory.Create_Gene((ushort)(msb + lsb));
                ++i0;
            }
            return gének;
        }
        byte[] rekonvert(IGene[] u)
        {
            List<byte> output = new List<byte>();
            for (int i = 0; i < fejlec.Length; i++)
            {
                output.Add(fejlec[i]);
            }

            for (int i = 0; i < u.Length; ++i)
            {
                output.Add((byte)(u[i]._TheNumber % 256));
                byte temp = (byte)(u[i]._TheNumber / 256);
                temp -= 128;
                output.Add(temp);
            }
            return output.ToArray();
        }



    }

    public class IOMap
    {
        static byte[] alapminta;
        public IOMap()
        {
            if(File.Exists("mitxcf.bmp")) alapminta = File.ReadAllBytes("mitxcf.bmp");
        }

        public bool[,] _ReadToBoolArray(string FileVagyMappanev, int határ)
        {
            byte[] terkep0 = File.ReadAllBytes(FileVagyMappanev);
            //alapminta = terkep0;

            int n = terkep0[10];
            int sz = terkep0[18];
            int m = terkep0[22];

            bool[,] terkep = new bool[m, sz];

            int k = m - 1; int j = 0;
            for (int i = n; i < terkep0.Length; i += 4)
            {
                if (terkep0[i] < határ)
                { terkep[k, j] = false; }
                else
                { terkep[k, j] = true; }
                j++; if (j == sz)
                { j = 0; --k; }
            }
            return terkep;
        }
        
        public int[,] _ReadToIntArray(string FileVagyMappanev, Csatorna csatorna)
        {
            byte[] terkep0 = File.ReadAllBytes(FileVagyMappanev);
            //alapminta = terkep0;

            int n = terkep0[10];
            int sz = terkep0[18] + terkep0[19]*256;
            int m = terkep0[22] + terkep0[23]*256;
            int rgb_or_rgba = (terkep0.Length - n) / (m * sz);

            int[,] terkep = new int[m, sz];

            int k = m - 1; int j = 0;
            for (int i = n + (int)csatorna; i < terkep0.Length; i += rgb_or_rgba)
            {
                terkep[k, j] = terkep0[i];

                j++; if (j == sz)
                { j = 0; --k; }
            }
            return terkep;
        }
        
        public void _Write(string Nev, DisplayMode mode, int[,] Map_Minta, int[,] Map_R = null, int[,] Map_G = null, int[,] Map_B = null)
            {
                Map_R = Normalizys(Map_R, mode);
                Map_G = Normalizys(Map_G, mode);
                Map_B = Normalizys(Map_B, mode);

                List<byte> terkep0 = new List<byte>();
                for (int i = 0; i < alapminta[10]; i++)
                {
                    terkep0.Add(alapminta[i]);
                }
                terkep0[18] = (byte)Map_Minta.GetLength(1);
                terkep0[19] = (byte)(Map_Minta.GetLength(1)/256);
                terkep0[22] = (byte)Map_Minta.GetLength(0);
                terkep0[23] = (byte)(Map_Minta.GetLength(0)/256);

                bool _4bit = (int)alapminta[28] == 32 ? true : false;

                for (int i = Map_Minta.GetLength(0) - 1; i > -1; --i)
                {
                    for (int j = 0; j < Map_Minta.GetLength(1); ++j)
                    {
                        terkep0.Add(Map_B == null ? (byte)0 : (byte)(Map_B[i, j]));
                        terkep0.Add(Map_G == null ? (byte)0 : (byte)(Map_G[i, j]));
                        terkep0.Add(Map_R == null ? (byte)0 : (byte)(Map_R[i, j]));
                        if (_4bit)terkep0.Add((byte)0);
                        
                    }
                }
                File.WriteAllBytes(Nev, terkep0.ToArray());
            }

        private int[,] Normalizys(int[,] array, DisplayMode mode)
        {
            if (array == null) return null;

            int min = int.MaxValue;
            int max = int.MinValue;
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (array[i, j] < min)
                    {
                        min = array[i, j];
                    }
                    if (array[i, j] > max)
                    {
                        max = array[i, j];
                    }
                }
            }
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    try
                    {
                        switch (mode)
                        {
                            case DisplayMode.Araanyos: array[i, j] = (255 * (array[i, j] - min)) / (max - min); break;
                            case DisplayMode.Logaritmikus: array[i, j] = (int)(((double)255) * Math.Log10(array[i, j] / Math.Log10(max))); break;
                            case DisplayMode.Logikai: array[i, j] = array[i, j] > 0 ? 255 : 0; break;
                        }
                    }
                    catch (DivideByZeroException)
                    {
                        array[i, j] = 0;
                    }
                }
            }
            return array;
        }
        
    }

    public enum Csatorna { Red = 2, Green = 1, Blue = 0 }
    public enum DisplayMode { Logikai, Araanyos, Logaritmikus }
}
