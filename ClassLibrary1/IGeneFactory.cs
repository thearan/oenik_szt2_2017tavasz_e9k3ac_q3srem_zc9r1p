﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    class IGeneFactory
    {
        public static IGene[] Create_Gene(ushort[] u, string c = "")
        {
            IGene[] output = new IGene[u.Length];
            for (int i = 0; i < u.Length; i++)
            {
                output[i] = new Gene(u[i], c);
            }
            return output;
        }

        public static IGene Create_Gene(ushort v)
        {
            return new Gene(v);
        }

        public static IGene Create_Gene(ushort u, string c)
        {
            return new Gene(u, c);
        }
    }
}
