﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using DataAccessLayer;

namespace BusinessLogic
{ 
 public struct Gene : IGene
    {
    ushort theNumber;

    public ushort _TheNumber
    {
        get { return theNumber; }
        set { theNumber = value; }
    }
    public Gene(int value, string cimke = "") { theNumber = (ushort)value; this.cimke = cimke; }
    public Gene(ushort value, string cimke) { theNumber = value; this.cimke = cimke;  }

    string cimke;

    public string _Cimke
    {
        get { return cimke; }
        set { cimke = value; }
    }



        //public static implicit operator ushort(Gene i)
        //{
        //    return i._TheNumber;
        //}
        //public static implicit operator Gene(ushort i)
        //{
        //    return new Gene(i, "") { theNumber = i, cimke = "" };
        //}
        //public static implicit operator int(Gene i)
        //{
        //    return (int)i._TheNumber;
        //}
        //public static implicit operator Gene(int i)
        //{
        //    return new Gene(i, "");
        //}

        public override string ToString()
    {
        return _TheNumber.ToString() + " " + cimke.Substring(cimke.LastIndexOf(@"\") + 1);
    }
}

}
//}
    

