﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class SimulationParameters
    {
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;
        static void OPC(string propertyName)
        {
            var handler = StaticPropertyChanged;
            if (handler != null)
                handler(null, new PropertyChangedEventArgs(propertyName));
        }

        static int n = 30;
        public static int N { get { return n; } set { n = value; OPC("N"); } }

        static double torzShape_szelekcio = 3;
        public static double _TorzShape_Szelekcio
        {
            get { return torzShape_szelekcio; }
            set { torzShape_szelekcio = value; OPC("_TorzShape_Szelekcio"); }
        }

        static int global_Maxlak = 10000;
        public static int _Global_Maxlak
        {
            get { return SimulationParameters.global_Maxlak; }
            set { SimulationParameters.global_Maxlak = value; OPC("_Global_Maxlak"); }
        }


        static int vandorlasi_maxTav = 10;
        public static int _Vandorlasi_maxTav
        {
            get { return SimulationParameters.vandorlasi_maxTav; }
            set { SimulationParameters.vandorlasi_maxTav = value; OPC("_Vandorlasi_maxTav"); }
        }

        static double vandorlas_valoszinusege = 10; // szazalek
        public static double _Vandorlas_valoszinusege
        {
            get { return SimulationParameters.vandorlas_valoszinusege; }
            set { SimulationParameters.vandorlas_valoszinusege = value; OPC("_Vandorlas_valoszinusege"); }
        }

        static double termekenyseg = 4;
        public static double _Termekenyseg
        {
            get { return termekenyseg; }
            set { termekenyseg = value; OPC("_Termekenyseg"); }
        }

        static double mutacios_Rata = 1; //  /születés
        public static double _Mutacios_Rata
        {
            get { return SimulationParameters.mutacios_Rata; }
            set { SimulationParameters.mutacios_Rata = value; OPC("_Mutacios_Rata"); }
        }

        static int mutacio_Erosseg = 256;
        public static int _Mutacio_Erosseg
        {
            get { return SimulationParameters.mutacio_Erosseg; }
            set { SimulationParameters.mutacio_Erosseg = value; OPC("_Mutacio_Erosseg"); }

        }

        static double jarvanyterjedesiHalalozasiArany = 25;
        public static double _JarvanyterjedesiHalalozasiArany
        {
            get { return SimulationParameters.jarvanyterjedesiHalalozasiArany; }
            set { SimulationParameters.jarvanyterjedesiHalalozasiArany = value; OPC("_JarvanyterjedesiHalalozasiArany"); }
        }

        static double belterjessegi_szelekcio_mertek = 0;
        public static double _Belterjessegi_szelekcio_mertek
        {
            get { return belterjessegi_szelekcio_mertek; }
            set { belterjessegi_szelekcio_mertek = value; OPC("_Belterjessegi_szelekcio_mertek"); }
        }


        static double torzColor_Szelekcio = 3;
        public static double _TorzColor_Szelekcio
        {
            get { return SimulationParameters.torzColor_Szelekcio; }
            set { SimulationParameters.torzColor_Szelekcio = value; OPC("_TorzColor_Szelekcio"); }
        }

        static double torzAsszim_Szelekcio = 3;
        public static double _TorzAsszim_Szelekcio
        {
            get { return SimulationParameters.torzAsszim_Szelekcio; }
            set { SimulationParameters.torzAsszim_Szelekcio = value; OPC("_TorzAsszim_Szelekcio"); }
        }










    }
}
