﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using System.Data.Entity;

namespace BusinessLogic
{
    public class DAL_Kezelo 
    {
        ICimkeRepo cimkerepo;
        IGeneRepo generepo;
        IEmberAdatRepo emberadatrepo;
        IQueries queries;
        private int embercounter;
        public int EmberCounter { get { return embercounter; } }


        public DAL_Kezelo(ICimkeRepo cimkerepo, IGeneRepo generepo, IEmberAdatRepo emberadatrepo)
        {
            this.cimkerepo = cimkerepo; 
            this.generepo = generepo;
            this.emberadatrepo = emberadatrepo;
            this.queries = IQueriesFactory.Create(cimkerepo, generepo, emberadatrepo);
        }
        public DAL_Kezelo()
        {
            DbContext dbc  = new GenebaseEntities();
            this.cimkerepo = ICimkeRepoFactory.Create(dbc);
            this.generepo = IGeneRepoFactory.Create(dbc);
            this.emberadatrepo = IEmberAdatRepoFactory.Create(dbc);
            this.queries = IQueriesFactory.Create(cimkerepo, generepo, emberadatrepo);
        }

        public void _AddEmber(Ember ember)
        {
            try 
            {
                embercounter = (int)queries.GetAllEMBERADAT().Max(x => x.id) + 1;
            }
            catch (InvalidOperationException)
            {
                embercounter = 0;
            }

            emberadatrepo.Insert(new EMBERADAT() { THE_CIMKE = ember._VegsoOs, NEM = ember._Nem, id = embercounter });
            int i;
            for ( i = 0; i < ember._Gének.Length; i++)
            {
                IGene gen = ember._Gének[i];
                generepo.Insert(IGENEFactory.Create(embercounter, i, gen._Cimke, gen._TheNumber));
            }
            for (int j = 0; i < ember._EllentetesNemiKromoszoma.Length + ember._Gének.Length; i++)
            {
                IGene gen = ember._EllentetesNemiKromoszoma[j];
                generepo.Insert(IGENEFactory.Create(embercounter, i, gen._Cimke, gen._TheNumber));
                ++j;
            }
        }
        public Ember _EmberElementAt(int index)
        {

            //EMBERADAT temp = queries.GetEmberAdatByID(i);
            List<IEMBERADAT> templist = queries.GetAllEMBERADAT().ToList();
            IEMBERADAT temp = templist[index];
            return ConvertEMBERADATToEmber(temp);
        }
        private Ember ConvertEMBERADATToEmber(IEMBERADAT temp)
        {
            IEnumerable<IGene> genekfull = from akt in queries.GetGenesOfEmber(temp)
                                           select IGeneFactory.Create_Gene((ushort)akt.THE_NUMBER, akt.THE_CIMKE);
            IEmberAdat output = IEmberAdatFactory.Create_EmberAdat(temp.THE_CIMKE, temp.NEM.Value, genekfull);

            return new Ember(output);
        }
        public IEnumerable<Ember> _GetAllEmber()
        {
            //return (from akt in emberadatrepo.GetAll()
            //        select ConvertEMBERADATToEmber(akt));


            IEnumerable<IEMBERADAT> emberadatok = queries.GetAllEMBERADAT();
            for (int i = 0; i < emberadatok.Count(); i++)
            {
                yield return ConvertEMBERADATToEmber(emberadatok.ElementAt(i));
            }
        }
        public void _DeleteEmber(int index)
        {

            //EMBERADAT temp = queries.GetEmberAdatByID(i);
            List<IEMBERADAT> templist = queries.GetAllEMBERADAT().ToList();
            IEMBERADAT temp = templist[index];
            emberadatrepo.Delete(temp as EMBERADAT);
        }
        public void _DeleteEmber(Ember e)
        {
            //EMBERADAT osztály példánya ideiglenes használatra
            EMBERADAT ea = new EMBERADAT() { THE_CIMKE = e._VegsoOs, NEM = e._Nem };
            for (int i = 0; i < e._Gének.Length; i++)
            {
                ea.GENE.Add(IGENEFactory.Create(e._Gének[i]._Cimke, e._Gének[i]._TheNumber));
            }
            for (int i = 0; i < e._EllentetesNemiKromoszoma.Length; i++)
            {
                ea.GENE.Add( IGENEFactory.Create(e._EllentetesNemiKromoszoma[i]._Cimke, e._EllentetesNemiKromoszoma[i]._TheNumber, (e._Gének.Length + i)));
            }
            List<IEMBERADAT> list = queries.GetAllEMBERADAT().ToList();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].EqualTo(ea))
                {
                    emberadatrepo.Delete(list[i] as EMBERADAT);
                }
            }
        }


        public IEnumerable<string> _CimkeListaToStringLista
        {
            get
            {
                return queries.CimkeListaToStringLista();
            }
        }
        public void _Add_Cimke(string cimke0)
        {
            bool condition = this._CimkeListaToStringLista.Contains(cimke0);
            if (!condition)
            {
                cimkerepo.Insert(ICIMKEFactory.Create(cimke0));
            }
        }

        public void DeleteAll()
        {
            generepo.DeleteAll();
            emberadatrepo.DeleteAll();
            cimkerepo.DeleteAll();
        }
    }
}
