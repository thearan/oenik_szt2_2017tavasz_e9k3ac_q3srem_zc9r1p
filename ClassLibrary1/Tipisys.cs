﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Tipisys
    {
        private List<Klaszter> Klaszterek;
        //nested class
        public class Klaszter : Kolonia
        {
            internal static int idcounter = 0;
            public int _KlaszterID;
            public int[,] _Elterjedes(Játéktér jt)
            {
                int[,] output = new int[jt._Koloniak.GetLength(0), jt._Koloniak.GetLength(1)];
                for (int i = 0; i < jt._Koloniak.GetLength(0); i++)
                {
                    for (int j = 0; j < jt._Koloniak.GetLength(1); j++)
                    {
                        if (jt._Koloniak[i, j] != null && jt._Koloniak[i, j]._Egyedek.Count > 0)
                        {
                            output[i, j] = this._Egyedek.Intersect(jt._Koloniak[i, j]._Egyedek).Count();
                        }
                    }
                }
                return output;
            }

            public override string ToString()
            { return String.Format("Klaszter-{0} , Egyedszám:{1}", _KlaszterID, _Lakossag); }

            public Klaszter()
            {
                idcounter++;
                _KlaszterID = idcounter;
            }
        }
        Játéktér jtktr;
        public Tipisys(Játéktér jt0)
        {
            jtktr = jt0;
            Klaszter.idcounter = 0;
            Klaszterek = new List<Klaszter>();
            
        }

        public enum TavSzamMode { EgyenesagiHaplocsoportokBeleszamitasaval, EgyenesagiHaplocsoportokBeleszamitasaNelkul }
        public void KlaszterezesInditas(TavSzamMode tavSzamMode)
        {
            #region 1D tömbbe olvasása egyedeknek
            List<Ember> ember_list = new List<Ember>();
            for (int i = 0; i < jtktr._Koloniak.GetLength(0); i++)
            {
                for (int j = 0; j < jtktr._Koloniak.GetLongLength(1); j++)
                {
                    if (jtktr._Koloniak[i, j] != null && jtktr._Koloniak[i, j]._Lakossag > 0)
                    {
                        foreach (var item in jtktr._Koloniak[i, j]._Egyedek)
                        {
                            ember_list.Add(item);
                        }
                    }
                }
            }
            Ember[] egyedek_tomb = ember_list.ToArray();
            #endregion

            #region mindenkihez a legközelebbi egyed megtalálása
            double min;

            for (int i = 0; i < egyedek_tomb.Length; i++)
            {
                min = double.MaxValue;
                for (int j = 0; j < egyedek_tomb.Length; j++)
                {
                    if (i != j)
                    {
                        switch (tavSzamMode)
                        {
                            case TavSzamMode.EgyenesagiHaplocsoportokBeleszamitasaval:
                                double temp = egyedek_tomb[i]._Genetikai_Tavolsag4zet1(egyedek_tomb[j]);
                                if (min > temp)
                                {
                                    min = temp;
                                    egyedek_tomb[i]._HozzamVanLegkozelebb = j;
                                }
                                break;
                            case TavSzamMode.EgyenesagiHaplocsoportokBeleszamitasaNelkul:
                                double temp2 = egyedek_tomb[i]._Genetikai_Tavolsag4zet2(egyedek_tomb[j]);
                                if (min > temp2)
                                {
                                    min = temp2;
                                    egyedek_tomb[i]._HozzamVanLegkozelebb = j;
                                }
                                break;
                        }
                    }
                }
            }
            #endregion

            #region egyedek legközelebbiszomszéd-gráfjának irányítatlan gráffá tétele
            for (int i = 0; i < egyedek_tomb.Length; i++)
            {
                egyedek_tomb[egyedek_tomb[i]._HozzamVanLegkozelebb]._HozzajukLegkozelebbEnVagyok.Add(i);
                if (!egyedek_tomb[i]._HozzajukLegkozelebbEnVagyok.Contains(egyedek_tomb[i]._HozzamVanLegkozelebb))
                {
                    egyedek_tomb[i]._HozzajukLegkozelebbEnVagyok.Add(egyedek_tomb[i]._HozzamVanLegkozelebb);
                }
            }
            #endregion

            #region komponensek megkeresése a legközelebbiszomszéd-gráfban
            List<Ember> mar_feldolgozott = new List<Ember>();

            for (int i = 0; i < egyedek_tomb.Length; i++)
            {
                mar_feldolgozott.Add(egyedek_tomb[i]);
                if (Klaszterek.Find(x => x._Egyedek.Contains(egyedek_tomb[i])) == null)
                {
                    Klaszter uj_klaszter = SzelessegiBejaras(egyedek_tomb[i], egyedek_tomb, mar_feldolgozott);
                    Klaszterek.Add(uj_klaszter);
                }
            }
            #endregion

            Klaszterek.OrderBy(x => x._Lakossag);
        }

        private Klaszter SzelessegiBejaras(Ember e, Ember[] e_tomb, List<Ember> mar_feldolgozott)
        {
            Queue<Ember> S = new Queue<Ember>();
            List<Ember> klaszter_egyedek = new List<Ember>();
            S.Enqueue(e);
            klaszter_egyedek.Add(e);

            while (S.Count() != 0)
            {
                Ember k;
                k = S.Dequeue();

                foreach (var x in k._HozzajukLegkozelebbEnVagyok)
                {
                    if (!klaszter_egyedek.Contains(e_tomb[x]))
                    {
                        S.Enqueue(e_tomb[x]);
                        klaszter_egyedek.Add(e_tomb[x]);
                        mar_feldolgozott.Add(e_tomb[x]);
                    }
                }
            }

            Klaszter uj_klaszter = new Klaszter();
            uj_klaszter._Bepakol(klaszter_egyedek);

            return uj_klaszter;
        }

        public Ember GetAtlag(int idx, bool nem)
        {
            //nem == true a férfi
            if (nem)
            {
                return Klaszterek.ElementAt(idx)._AtlagArc_Ferfi();
            }
            else
            {
                return Klaszterek.ElementAt(idx)._AtlagArc_No();
            }
        }

        public int[,] GetElterjedes(int idx)
        {
            return Klaszterek.ElementAt(idx)._Elterjedes(jtktr);
        }

        public int GetEgyedSzam(int idx)
        {
            return Klaszterek.ElementAt(idx)._Lakossag;
        }

        public string GetKlaszterToString(int idx)
        {
            return Klaszterek.ElementAt(idx).ToString();
        }

        public double GetHeterogenitas(int idx)
        {
            return Klaszterek[idx]._Heterogenitas();
        }

        public int KlaszterekSzama { get { return Klaszterek.Count; } }




    }
}
