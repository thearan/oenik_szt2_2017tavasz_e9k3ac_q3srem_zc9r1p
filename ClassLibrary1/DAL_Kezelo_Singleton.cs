﻿using System;

namespace BusinessLogic
{
    public class DAL_Kezelo_Singleton
    {
        private static DAL_Kezelo dalkezelo = new DAL_Kezelo();

        public static DAL_Kezelo _GetInstance()
        {
            return dalkezelo;
        }

        internal static void _Add_Cimke(string v)
        {
            dalkezelo._Add_Cimke(v);
        }
    }
}