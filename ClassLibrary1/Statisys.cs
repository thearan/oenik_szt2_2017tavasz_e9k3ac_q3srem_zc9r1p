﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public static class StatisysExtensions
    {
        public static Ember _AtlagArc_Ferfi(this Kolonia thisfalu)
        {
            var osszes_ferfi_gen = from x in thisfalu._Egyedek
                                   where x._Nem == true
                                   select x._Gének;

            var osszes_ferfi_ellentetes_nk = from x in thisfalu._Egyedek
                                             where x._Nem == true
                                             select x._EllentetesNemiKromoszoma;

            int ferfi_egyedek_szama = thisfalu._Egyedek.Where(e => e._Nem == true).Count();
            var nk_meret = 14;
            var g_meret = 130;

            IGene[] atlag_nk = new IGene[nk_meret]; ////EllentétesNemiKromoszóma számára
            IGene[] atlag_gen = new IGene[g_meret]; ////Gének számára

            for (int i = 0; i < nk_meret; i++)
            {
                atlag_nk[i] = IGeneFactory.Create_Gene(0);
            }

            for (int i = 0; i < g_meret; i++)
            {
                atlag_gen[i] = IGeneFactory.Create_Gene(0);
            }

            foreach (var nk in osszes_ferfi_ellentetes_nk)
            {
                for (int i = 0; i < nk.Length; i++)
                {
                    atlag_nk[i]._TheNumber += (ushort)(nk[i]._TheNumber / (ushort)ferfi_egyedek_szama);
                }
            }

            foreach (var gn in osszes_ferfi_gen)
            {
                for (int i = 0; i < gn.Length; i++)
                {
                    atlag_gen[i]._TheNumber += (ushort)(gn[i]._TheNumber / (ushort)ferfi_egyedek_szama);
                }
            }

            EmberAdat ea = new EmberAdat();
            ea.ellentetesNemiKromoszoma = atlag_nk;
            ea.gének = atlag_gen;
            ea.cimke = "f";
            Ember atlagolt_arcu_ferfi = new Ember(ea);

            return atlagolt_arcu_ferfi;
        }

        public static Ember _AtlagArc_No(this Kolonia thisfalu)
        {
            var osszes_noi_gen = from x in thisfalu._Egyedek
                                 where x._Nem == false
                                 select x._Gének;

            var osszes_noi_ellentetes_nk = from x in thisfalu._Egyedek
                                           where x._Nem == false
                                           select x._EllentetesNemiKromoszoma;

            var nk_meret = 14;
            var g_meret = 130;

            int noi_egyedek_szama = thisfalu._Egyedek.Where(e => e._Nem == false).Count();
            IGene[] atlag_gen = new IGene[g_meret]; ////Gének számára
            IGene[] atlag_nk = new IGene[nk_meret]; ////EllentetesNemiKromoszoma számára

            for (int i = 0; i < nk_meret; i++)
            {
                atlag_nk[i] = IGeneFactory.Create_Gene(0);
            }

            for (int i = 0; i < g_meret; i++)
            {
                atlag_gen[i] = IGeneFactory.Create_Gene(0);
            }

            foreach (var nk in osszes_noi_ellentetes_nk)
            {
                for (int i = 0; i < nk.Length; i++)
                {
                    atlag_nk[i]._TheNumber += (ushort)(nk[i]._TheNumber / (ushort)noi_egyedek_szama);
                }
            }

            foreach (var gn in osszes_noi_gen)
            {
                for (int i = 0; i < gn.Length; i++)
                {
                    atlag_gen[i]._TheNumber += (ushort)(gn[i]._TheNumber / (ushort)noi_egyedek_szama);
                }
            }

            EmberAdat ea = new EmberAdat();
            ea.ellentetesNemiKromoszoma = atlag_nk;
            ea.gének = atlag_gen;
            ea.cimke = "n";
            Ember atlagolt_arcu_no = new Ember(ea);

            return atlagolt_arcu_no;
        }

        public static double _Heterogenitas(this Kolonia thisfalu)

        {
            if (thisfalu == null || thisfalu._Lakossag == 0)
            {
                return 0;
            }

            var osszes_ferfi = from x in thisfalu._Egyedek
                               where x._Nem == true
                               select x;

            Ember atlagarcu_ferfi = _AtlagArc_Ferfi(thisfalu);
            double ferfi_tavolsagok_negyzet_osszege = 0;

            var osszes_no = from x in thisfalu._Egyedek
                            where x._Nem == false
                            select x;

            Ember atlagarcu_no = _AtlagArc_No(thisfalu);
            double noi_tavolsagok_negyzet_osszege = 0;

            foreach (var no in osszes_no)
            {
                noi_tavolsagok_negyzet_osszege += no._Genetikai_Tavolsag4zet1(atlagarcu_no);
            }

            foreach (var ferfi in osszes_ferfi)
            {
                ferfi_tavolsagok_negyzet_osszege += ferfi._Genetikai_Tavolsag4zet1(atlagarcu_ferfi);
            }


            double sum = ferfi_tavolsagok_negyzet_osszege + noi_tavolsagok_negyzet_osszege;
            double h = Math.Sqrt(sum / thisfalu._Egyedek.Count());

            return h;
        }

        public static double _FerfiAgiLeszarmazottakAranya(this Kolonia thisfalu, string adottcimke)
        {
            if (thisfalu == null || thisfalu._Lakossag == 0)
            {
                return 0;
            }

            var ferfi_egyedek = from x in thisfalu._Egyedek
                                where x._Nem == true
                                select x;

            int ferfiak_szama = ferfi_egyedek.Count();
            int szamlalo = 0;

            foreach (var item in ferfi_egyedek)
            {
                if (item._VegsoOs == adottcimke)
                {
                    szamlalo++;
                }
            }

            if (ferfiak_szama == 0)
            {
                return 0;
            }

            double szazalekos_arany = ((double)szamlalo / (double)ferfiak_szama) * 100;

            return szazalekos_arany;
        }

        public static double _NoiAgiLeszarmazottakAranya(this Kolonia thisfalu, string adottcimke)
        {
            if (thisfalu == null || thisfalu._Lakossag == 0)
            {
                return 0;
            }

            var noi_egyedek = from x in thisfalu._Egyedek
                              where x._Nem == false
                              select x;

            int nok_szama = noi_egyedek.Count();
            int szamlalo = 0;

            foreach (var item in noi_egyedek)
            {
                if (item._VegsoOs == adottcimke)
                {
                    szamlalo++;
                }
            }

            if (nok_szama == 0)
            {
                return 0;
            }

            double szazalekos_arany = (double)((double)szamlalo / (double)nok_szama) * 100;

            return szazalekos_arany;
        }

        public static double _SzarmazasiAraany(this Ember e, string adottcimke)
        {
            int gk = e._Gének.Length;   ////gén készlet
            int ekszm = e._EllentetesNemiKromoszoma.Length; ////ellentétes nemi kromoszóma génkészlete
            int szamlalo = 0;

            foreach (var item in e._Gének)
            {
                if (item._Cimke == adottcimke)
                {
                    szamlalo++;
                }
            }

            foreach (var item in e._EllentetesNemiKromoszoma)
            {
                if (item._Cimke == adottcimke)
                {
                    szamlalo++;
                }
            }

            double nevezo = (double)(gk) + (double)(ekszm);
            if (nevezo == 0) { return 0; }
            double szarmazasi_arany = (szamlalo / nevezo) * 100;

            return szarmazasi_arany;
        }

        public static double _SzarmazasiAraany(this Kolonia thisfalu, string adottcimke)
        {
            if (thisfalu == null || thisfalu._Lakossag == 0) { return 0; }

            double arany = 0;

            foreach (var egyed in thisfalu._Egyedek)
            {
                arany += _SzarmazasiAraany(egyed, adottcimke);
            }

            double adott_cimkejuek_szazaleka = (arany / (double)thisfalu._Lakossag);

            return adott_cimkejuek_szazaleka;
        }
    }

    public class Statisys
    {
        private Játéktér játéktér;
        private int x;
        private int y;
        private int[,] output;

        public Statisys(Játéktér játéktér)
        {
            this.játéktér = játéktér;
            this.x = játéktér._Koloniak.GetLength(0);
            this.y = játéktér._Koloniak.GetLength(1);
            this.output = new int[x, y];
        }

        public int[,] Tipizáció(int[,] non_normalized_output)
        {
            return Normalizys(non_normalized_output);
        }

        public int[,] NepessegEloszlas()
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (játéktér._Koloniak[i, j] != null)
                        output[i, j] = (int)játéktér._Koloniak[i, j]._Lakossag;
                    else
                        output[i, j] = 0;
                }
            }
            return Normalizys(output);
        }

        public int[,] Heterogenitas_to_Map()
        {
            int x = játéktér._Koloniak.GetLength(0);
            int y = játéktér._Koloniak.GetLength(1);

            int[,] output = new int[x, y];

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    output[i, j] = (int)játéktér._Koloniak[i, j]._Heterogenitas();
                }
            }

            output = this.Normalizys(output);

            return output;
        }

        public int[,] _FerfiAgiLeszarmazottakAranya_to_Map(string adottcimke)
        {
            int x = játéktér._Koloniak.GetLength(0);
            int y = játéktér._Koloniak.GetLength(1);

            int[,] output = new int[x, y];

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    output[i, j] = (int)játéktér._Koloniak[i, j]._FerfiAgiLeszarmazottakAranya(adottcimke);
                }
            }

            output = this.Normalizys(output);

            return output;
        }

        public int[,] _NoiAgiLeszarmazottakAranya_to_Map(string adottcimke)
        {
            int x = játéktér._Koloniak.GetLength(0);
            int y = játéktér._Koloniak.GetLength(1);

            int[,] output = new int[x, y];

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    output[i, j] = (int)játéktér._Koloniak[i, j]._NoiAgiLeszarmazottakAranya(adottcimke);
                }
            }

            output = this.Normalizys(output);

            return output;
        }

        public int[,] _SzarmazasiArany_to_Map(string adottcimke)
        {
            int x = this.játéktér._Koloniak.GetLength(0);
            int y = this.játéktér._Koloniak.GetLength(1);

            int[,] output = new int[x, y];

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    output[i, j] = (int)this.játéktér._Koloniak[i, j]._SzarmazasiAraany(adottcimke);
                }
            }

            output = this.Normalizys(output);

            return output;
        }

        private int[,] Normalizys(int[,] array, DisplayMode mode = DisplayMode.Araanyos)
        {
            if (array == null)
            {
                return null;
            }

            if (this.játéktér._Full_Lakossag == 0)
            {
                return null;
            }

            int min = int.MaxValue;
            int max = 1;
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (array[i, j] > max)
                    {
                        max = array[i, j];
                    }
                    else if (array[i, j] < min && array[i, j] != 0)
                    {
                        min = array[i, j];
                    }
                }
            }

            if (max != min)
            {
                for (int i = 0; i < array.GetLength(0); i++)
                {
                    for (int j = 0; j < array.GetLength(1); j++)
                    {
                        switch (mode)
                        {
                            case DisplayMode.Araanyos:
                                if (array[i, j] != 0)
                                {
                                    array[i, j] = (int)((255 * (array[i, j] - min)) / (double)(max - min));
                                }
                                break;
                            case DisplayMode.Logaritmikus: array[i, j] = (int)(((double)255) * Math.Log10(array[i, j] / Math.Log10(max))); break;
                            case DisplayMode.Logikai: array[i, j] = array[i, j] > 0 ? 255 : 0; break;
                        }
                    }
                }
            }

            return array;
        }
    }
}
