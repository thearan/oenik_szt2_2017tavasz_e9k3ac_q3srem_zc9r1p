﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class NotifyGUI : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OPC([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class Játéktér : NotifyGUI
    {
        bool[,] tabla;
        public int _LakottKoloniakSzama
        {
            get
            {
                int db = 0;
                for (int i = 0; i < _Koloniak.GetLength(0); i++)
                {
                    for (int j = 0; j < _Koloniak.GetLength(1); j++)
                    {
                        if (_Koloniak[i, j] != null && _Koloniak[i, j]._Lakossag != 0)
                            db++;
                    }
                }
                return db;
            }
        }
        int koloniakSzama = 0;
        public int _KoloniakSzama { get { return koloniakSzama; } }
        int full_Lakossag = 0;
        public int _Full_Lakossag
        {
            get
            {
                return full_Lakossag;
            }
            set
            {
                full_Lakossag = value;
                OPC();
            }
        }


        public Kolonia[,] _Koloniak { get; private set; }
        public Játéktér(bool[,] tabla)
        {
            this.tabla = tabla;
            _Koloniak = new Kolonia[tabla.GetLength(0), tabla.GetLength(1)];
            for (int i = 0; i < tabla.GetLength(0); i++)
            {
                for (int j = 0; j < tabla.GetLength(1); j++)
                {
                    if (tabla[i, j]) { _Koloniak[i, j] = new Kolonia(); koloniakSzama++; }
                    else { _Koloniak[i, j] = null; }
                }
            }

            for (int i = 0; i < tabla.GetLength(0); i++)
            {
                for (int j = 0; j < tabla.GetLength(1); j++)
                {
                    if (_Koloniak[i, j] != null)
                    {
                        try { _Koloniak[i, j].X_Felsoszomszed = _Koloniak[i - 1, j]; }
                        catch (IndexOutOfRangeException) { _Koloniak[i, j].X_Felsoszomszed = null; }

                        try { _Koloniak[i, j].X_Jobbszomszed = _Koloniak[i, j + 1]; }
                        catch (IndexOutOfRangeException) { _Koloniak[i, j].X_Jobbszomszed = null; }

                        try { _Koloniak[i, j].X_Alsoszomszed = _Koloniak[i + 1, j]; }
                        catch (IndexOutOfRangeException) { _Koloniak[i, j].X_Alsoszomszed = null; }

                        try { _Koloniak[i, j].X_Balszomszed = _Koloniak[i, j - 1]; }
                        catch (IndexOutOfRangeException) { _Koloniak[i, j].X_Balszomszed = null; }
                    }
                }
            }

        }
        public void _Elhelyez(List<Ember> egyedek, int YKoordinatys, int XKoordinatys)
        {
            if (_Koloniak[YKoordinatys, XKoordinatys] != null)
            {
                _Koloniak[YKoordinatys, XKoordinatys]._Bepakol(egyedek);
                this._Full_Lakossag += egyedek.Count;
            }
        }
        public void _NextStatys()
        {
            _Full_Lakossag = 0;
            #region Táblabejárás
            for (int i = 0; i < this._Koloniak.GetLength(0); i++)
            {
                for (int j = 0; j < this._Koloniak.GetLength(1); j++)
                {
                    if ((_Koloniak[i, j] != null && _Koloniak[i, j]._Lakossag != 0))
                    {
                        #endregion
                        #region Egyedek vándoroltatása
                        this.vandoroltat1(i, j);
                    }
                }
            }
            #endregion
            #region Táblabejárás
            for (int i = 0; i < this._Koloniak.GetLength(0); i++)
            {
                for (int j = 0; j < this._Koloniak.GetLength(1); j++)
                {
                    if (this._Koloniak[i, j] != null && this._Koloniak[i, j]._Lakossag > 0)
                    {
                        #endregion
                        #region Egyedek szaporíttatása
                        this._Koloniak[i, j]._Genesys();
                        _Full_Lakossag += this._Koloniak[i, j]._Lakossag;
                    }
                }
            }
            #endregion

            while (this._Full_Lakossag > SimulationParameters._Global_Maxlak)
            {
                #region Legnépesebb kolónia kiválasztása (maximumkiválasztás)
                int imax = 0; int jmax = 0;
                for (int i = 0; i < this._Koloniak.GetLength(0); i++)
                {
                    for (int j = 0; j < this._Koloniak.GetLength(1); j++)
                    {
                        if (this._Koloniak[i, j] != null)
                        {
                            int maxlak = 0;
                            if (this._Koloniak[imax, jmax] != null)
                            { maxlak = this._Koloniak[imax, jmax]._Lakossag; }
                            else { maxlak = 0; }

                            if (maxlak < this._Koloniak[i, j]._Lakossag) { imax = i; jmax = j; }
                        }
                    }
                }
                #endregion
                #region Járvány terjesztése legnépesebb kolóniából kiindulva
                int falumaxlak = SimulationParameters._Global_Maxlak / this.koloniakSzama;
                genocyd_hullamterjedes(_Koloniak[imax, jmax], falumaxlak);
                #endregion
            }
        }

        private void vandoroltat1(int i, int j)
        {
            #region Generálunk egy random települést az aktuális település közelében
            int i2 = i + Rnd._Next(-SimulationParameters._Vandorlasi_maxTav, SimulationParameters._Vandorlasi_maxTav + 1);
            int j2 = j + Rnd._Next(-SimulationParameters._Vandorlasi_maxTav, SimulationParameters._Vandorlasi_maxTav + 1);
            #endregion
            #region Ellenőrizzük telepíthetünk-e oda bevándorlókat
            if (i2 >= 0 &&
                j2 >= 0 &&
                i2 < _Koloniak.GetLength(0) &&
                j2 < _Koloniak.GetLength(1) &&
                _Koloniak[i2, j2] != null &&
                _Koloniak[i, j]._Lakossag > _Koloniak[i2, j2]._Lakossag)
            #endregion
            #region bejárjuk az kolónia embereit és random kiválasztunk néhányat akik átvándorolnak
            {
                for (int g = 0; g < _Koloniak[i, j]._Lakossag; g++)
                {
                    if (Rnd._Random_Valoszinuseg(SimulationParameters._Vandorlas_valoszinusege))
                    {
                        Ember etemp = _Koloniak[i, j]._Egyedek[g];
                        this._Koloniak[i2, j2]._Egyedek.Add(etemp);
                        this._Koloniak[i, j]._Egyedek.Remove(etemp);
                    }
                }
            }
            #endregion
        }
        private void genocyd_hullamterjedes(Kolonia kolonia, int falumaxlak)
        {
            #region kijeloles
            List<Kolonia> irtando = new List<Kolonia>() { kolonia };
            int grow = 0;
            do
            {
                grow = irtando.Count;
                for (int i = 0; i < irtando.Count; i++)
                {
                    Kolonia temp = irtando[i].X_Alsoszomszed;
                    if (temp != null &&
                         temp._Lakossag > falumaxlak &&
                         !irtando.Contains(temp))
                    {
                        irtando.Add(temp);
                    }


                    temp = irtando[i].X_Balszomszed;
                    if (temp != null &&
                         temp._Lakossag > falumaxlak &&
                         !irtando.Contains(temp))
                    {
                        irtando.Add(temp);
                    }


                    temp = irtando[i].X_Felsoszomszed;
                    if (temp != null &&
                        temp._Lakossag > falumaxlak &&
                        !irtando.Contains(temp))
                    {
                        irtando.Add(temp);
                    }


                    temp = irtando[i].X_Jobbszomszed;
                    if (temp != null &&
                        temp._Lakossag > falumaxlak &&
                        !irtando.Contains(temp))
                    {
                        irtando.Add(temp);
                    }
                }
            }
            while (grow != irtando.Count);



            #endregion

            for (int i = 0; i < irtando.Count; i++)
            {
                irtando[i]._Genocyd_lokal(falumaxlak, ref full_Lakossag);
            }
        }
        public override string ToString()
        {
            return String.Format("Játéktér; Lakosság:{0}", _Full_Lakossag);
        }


    }
}
