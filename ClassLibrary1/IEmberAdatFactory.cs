﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic
{
    public class IEmberAdatFactory
    {
        private static int[] nemiKromoszomaLokuszok = new int[] { 2, 3, 5, 7, 8, 9, 13, 80, 82, 83, 84, 86, 97, 111 };

        public static IEmberAdat Create_EmberAdat(string tHE_CIMKE, bool value, IEnumerable<IGene> genekfull)
        {
            IGene[] maingenes = new IGene[130];
            IGene[] nemikromoszoma = new IGene[nemiKromoszomaLokuszok.Length];
            IGene[] ellentetesnemikromoszoma = new IGene[nemiKromoszomaLokuszok.Length];

            for (int i = 0; i < 130; i++)
            {
                maingenes[i] = genekfull.ElementAt(i);
            }

            for (int i = 0; i < nemiKromoszomaLokuszok.Length; ++i)
            {
                nemikromoszoma[i] = genekfull.ElementAt(nemiKromoszomaLokuszok[i]);
            }

            int j = 0;
            for (int i = 130; i < genekfull.Count(); ++i)
            {
                ellentetesnemikromoszoma[j] = genekfull.ElementAt(i);
                ++j;
            }

            EmberAdat output = new EmberAdat()
            {
                cimke = tHE_CIMKE, nem = value,
                gének = maingenes,
                nemikromoszoma = nemikromoszoma,
                ellentetesNemiKromoszoma = ellentetesnemikromoszoma
            };
            return output;
        }

        internal static IEmberAdat Create_EmberAdat(IGene[] gene, string v)
        {
            return new EmberAdat(gene, v);
        }

        internal static IEmberAdat Create_EmberAdat(IGene[] tempgenek, string tempvegsoos, bool v)
        {
            return new EmberAdat(tempgenek, tempvegsoos, v);
        }
    }
}