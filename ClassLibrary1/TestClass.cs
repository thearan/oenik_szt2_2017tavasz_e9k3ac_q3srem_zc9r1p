﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using NUnit.Framework;
using System.IO;
using DataAccessLayer;

namespace BusinessLogic
{
    [TestFixture]
    public class Test
    {

        [Test]
        public void MainTestBL()
        {
            Assert.That( ()=>Simulation() , Throws.Nothing);
        }

        public void Simulation()
        {
            #region arrange
            string dir = @"D:\kimentendo\Documents\Visual Studio 2015\Projects\WpfApplication1\WpfApplication1\bin\Debug\";
            string dir2 = dir + @"TestMappa\";
            //if (Directory.Exists(dir2)) { Directory.Delete(dir2, true); }
            //Directory.CreateDirectory(dir2);
            Directory.SetCurrentDirectory(dir);

            Játéktér játéktér = new Játéktér(new IOMap()._ReadToBoolArray("mitxcf.bmp", 127));
            IO_Ember ioe = new IO_Ember();
            List<Ember> LE = new List<Ember>();
            List<IEmberAdat> g = ioe._Read("mintaarcok");
            #endregion
            #region act
            for (int i = 0; i < g.Count; i++)
            {
                LE.Add(new Ember(g[i]));
            }
            játéktér._Elhelyez(LE, 17, 34);

            for (int u = 0; u < 100; u++)
            {
                játéktér._NextStatys();
                #endregion

                #region "Assert"
                #region térképkiköpködés
                IOMap iom = new IOMap();
                int[,] tombint = new int[játéktér._Koloniak.GetLength(0), játéktér._Koloniak.GetLength(1)];
                for (int i = 0; i < játéktér._Koloniak.GetLength(0); i++)
                {
                    for (int j = 0; j < játéktér._Koloniak.GetLength(1); j++)
                    {
                        if (játéktér._Koloniak[i, j] != null)
                        {
                            tombint[i, j] = játéktér._Koloniak[i, j]._Lakossag;
                        }
                        else
                        { tombint[i, j] = 0; }
                    }
                }
                iom._Write(String.Format("{1}Elterjedes{0}.bmp", u, dir2), DisplayMode.Logikai, tombint, tombint, new IOMap()._ReadToIntArray("mitxcf.bmp", Csatorna.Green));
                #endregion
            }
            #region egyedkiköpködés
            int imax = 0; int jmax = 0;
            for (int i = 0; i < játéktér._Koloniak.GetLength(0); i++)
            {
                for (int j = 0; j < játéktér._Koloniak.GetLength(1); j++)
                {
                    if (játéktér._Koloniak[i, j] != null)
                    {
                        int maxlak = 0;
                        try { maxlak = játéktér._Koloniak[imax, jmax]._Lakossag; }
                        catch (NullReferenceException) { maxlak = 0; }

                        if (maxlak < játéktér._Koloniak[i, j]._Lakossag) { imax = i; jmax = j; }
                    }
                }
            }
            for (int i = 0; játéktér._Koloniak[imax, jmax] != null && i < játéktér._Koloniak[imax, jmax]._Egyedek.Count; i++)
            {
                ioe._Write(játéktér._Koloniak[imax, jmax]._Egyedek[i]._Gének, String.Format("{1} {0}.fg", i, dir2 + játéktér._Koloniak[imax, jmax]._Egyedek[i].ToString()));
            }
            #endregion
            #endregion

        }

        [Test]
        public void OuterDALTest()
        {
            #region Arrange
            DAL_Kezelo dk = DAL_Kezelo_Singleton._GetInstance();
            dk.DeleteAll();
            string dir = @"D:\kimentendo\Documents\Visual Studio 2015\Projects\WpfApplication1\WpfApplication1\bin\Debug\";
            string dir2 = dir + @"TestMappa\";
            Directory.SetCurrentDirectory(dir);
            IO_Ember ioe = new IO_Ember(); // beolvas +1 cimkét!!
            #endregion

            #region act
            dk._Add_Cimke("cimke0");//első cimke
            dk._Add_Cimke("cimke1");//második cimke
            dk._Add_Cimke("cimke2");//harmadik cimke
            IEmberAdat ea = ioe._Read(@"mintaarcok\f1f.fg")[0]; //beolvas +1 cimkét!
            IEmberAdat ea2 = ioe._Read(@"mintaarcok\n1n.fg")[0]; //beolvas +1 cimkét!
            IEmberAdat ea3 = ioe._Read(@"mintaarcok\n1n.fg")[0]; 
            dk._AddEmber(new Ember(ea));//Hozzáadja az első embert
            dk._AddEmber(new Ember(ea2));//hozzáadja a második embert
            dk._AddEmber(new Ember(ea3));//hozzáadja a 3.embert
            Ember e = dk._EmberElementAt(0);
            ioe._Write(e._Gének, @"TestMappa\probaarc"); //Ugyan azt a fájlt adta vissza amit beolvasott pár sorral feljebb!!
            dk._DeleteEmber(e); //törli a első első embert
            dk._DeleteEmber(0); //törli az második embert



            var q = dk._CimkeListaToStringLista;
            var q3 = dk._GetAllEmber();
            ioe._Write(q3.Last()._Gének, @"TestMappa\probaarc_B");
            #endregion

            #region assert
            Assert.That(q.Count(), Is.EqualTo(3 + 3));
            Assert.That(q3.Count(), Is.EqualTo(1));
            #endregion

        }

        [Test]
        public void StatisysExtensionTest()
        {
            string dir = @"D:\kimentendo\Documents\Visual Studio 2015\Projects\WpfApplication1\WpfApplication1\bin\Debug\";
            string dir2 = dir + @"TestMappa\";
            IO_Ember ioe = new IO_Ember();
            List<IEmberAdat> g = ioe._Read(dir + "mintaarcok");
            List<IEmberAdat> g2 = ioe._Read(dir + "mintaarcok2");
            List<Ember> LE = (from x in g select new Ember(x)).ToList();
            List<Ember> LE2 = (from x in g2 select new Ember(x)).ToList();
            Kolonia k = new Kolonia();
            Kolonia k2 = new Kolonia();
            k._Bepakol(LE);
            k2._Bepakol(LE2);

            #region átlagarc
            Ember e = k._AtlagArc_Ferfi();
            Ember e2 = k._AtlagArc_No();
            ioe._Write(e._Gének, dir2 + "atlagf.fg");
            ioe._Write(e2._Gének, dir2 + "atlagn.fg");
            #endregion

            #region heterogenitás
            double h1 = k._Heterogenitas();
            double h2 = k2._Heterogenitas();
            System.Threading.Thread.Sleep(1);
            #endregion

            #region egyenesági származás
            double flszrm = k._FerfiAgiLeszarmazottakAranya("f1f");
            double nlszrm = k._NoiAgiLeszarmazottakAranya("n1n");
            #endregion


            #region teljes származási arány
            string[] ellenorzes = DAL_Kezelo_Singleton._GetInstance()._CimkeListaToStringLista.ToArray();
            double[] szarmaranyok = (from x in DAL_Kezelo_Singleton._GetInstance()._CimkeListaToStringLista
                                     where k._Egyedek.Select(y => y._VegsoOs).Contains(x)
                                     select k._SzarmazasiAraany(x)).ToArray();

           
            #endregion

            Assert.That(h1 > h2, Is.EqualTo(true));
            Assert.That(flszrm == nlszrm && flszrm == ((double)100 / (double)9), Is.EqualTo(true));
            Assert.That(szarmaranyok.All(y => y == ((double)100 / szarmaranyok.Length)), Is.EqualTo(true));

        }

        [Test]
        public void StatisysTest()
        {
            string dir = @"D:\kimentendo\Documents\Visual Studio 2015\Projects\WpfApplication1\WpfApplication1\bin\Debug\";
            string dir2 = dir + @"TestMappa\";
            //if (Directory.Exists(dir2)) { Directory.Delete(dir2, true); }
            //Directory.CreateDirectory(dir2);
            Directory.SetCurrentDirectory(dir);

            Játéktér játéktér = new Játéktér(new IOMap()._ReadToBoolArray("mitxcf.bmp", 127));
            IO_Ember ioe = new IO_Ember();
            List<Ember> LE = new List<Ember>();
            List<IEmberAdat> g = ioe._Read("mintaarcok");

            for (int i = 0; i < g.Count; i++)
            {
                LE.Add(new Ember(g[i]));
            }
            játéktér._Elhelyez(LE, 17, 34);

            for (int u = 0; u < 30; u++)
            {
                játéktér._NextStatys();
            }

            Statisys stat = new Statisys(játéktér);

            int[,] htrg = stat.Heterogenitas_to_Map();
            IEnumerable<int[,]> fglszrm = (from x in DAL_Kezelo_Singleton._GetInstance()._CimkeListaToStringLista
                                           select stat._FerfiAgiLeszarmazottakAranya_to_Map(x));
            IEnumerable<int[,]> nglszrm = (from x in DAL_Kezelo_Singleton._GetInstance()._CimkeListaToStringLista
                                           select stat._NoiAgiLeszarmazottakAranya_to_Map(x));
            IEnumerable<int[,]> szrmarany = (from x in DAL_Kezelo_Singleton._GetInstance()._CimkeListaToStringLista
                                           select stat._SzarmazasiArany_to_Map(x));
            List<int[,]> output = fglszrm.Union(nglszrm).Union(szrmarany).ToList();
            output.Add(htrg);

            IOMap io = new IOMap();
            int grr = 0;
            foreach (var item in output)
            {
                io._Write(dir2 + "trkp" + grr + ".bmp", DisplayMode.Araanyos , item , item);
                ++grr;
            }




        }

        [Test]
        public void KlaszterTeszt()
        {
            string dir = @"D:\kimentendo\Documents\Visual Studio 2015\Projects\WpfApplication1\WpfApplication1\bin\Debug\";
            string dir2 = dir + @"TestMappa\";
            Directory.SetCurrentDirectory(dir);

            Játéktér játéktér = new Játéktér(new IOMap()._ReadToBoolArray("mitxcf.bmp", 127));
            IO_Ember ioe = new IO_Ember();
            List<Ember> LE = new List<Ember>();
            List<IEmberAdat> g = ioe._Read("mintaarcok");

            for (int i = 0; i < g.Count; i++)
            {
                LE.Add(new Ember(g[i]));
            }
            játéktér._Elhelyez(LE, 17, 34);

            for (int u = 0; u < 30; u++)
            {
                játéktér._NextStatys();
            }

            Tipisys typ = new Tipisys(játéktér);

            typ.KlaszterezesInditas( Tipisys.TavSzamMode.EgyenesagiHaplocsoportokBeleszamitasaNelkul);


            List<int> temp = new List<int>();
            for (int i = 0; i < typ.KlaszterekSzama; i++)
            {
                temp.Add((int)typ.GetHeterogenitas(i));
            }

            double temp2 = 0;
            foreach (var item in játéktér._Koloniak)
            {
                temp2 += Math.Pow(item._Heterogenitas(), 2);
            }
            temp2 = Math.Sqrt(temp2);


            double temp3 = 0;
            for (int i = 0; i < typ.KlaszterekSzama; i++)
            {
                temp3 += typ.GetEgyedSzam(i);
            }


            IOMap iom = new IOMap();
            for (int i = 0; i < typ.KlaszterekSzama; i++)
            {
                int[,] temp666 = typ.GetElterjedes(i);
                iom._Write(dir2 + "trkp" + i + ".bmp", DisplayMode.Araanyos, temp666, temp666);
            }

            Assert.That(temp3, Is.EqualTo(játéktér._Full_Lakossag));
            Assert.That(temp.All(x => x < temp2), Is.EqualTo(true));


            

        }
    


    }
}
