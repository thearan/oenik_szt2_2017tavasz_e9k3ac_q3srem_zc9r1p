﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
    public class MintaEmber : NotifyUIBase
    {
        public string Filename { get; private set; }
        public int Index { get; set; }
        bool chk;
        public bool Checked
        {
            get { return chk; }
            set { chk = value; OPC(); }
        }

        public MintaEmber(string filename, bool chk)
        {
            Filename = filename;
            Checked = chk;
        }

        public MintaEmber(string filename, bool chk, int index)
        {
            Filename = filename;
            Checked = chk;
            Index = index;
        }

    }
}
