﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLogic;
using System.IO;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using Microsoft.Win32;
using System.Collections.ObjectModel;
using static BusinessLogic.Tipisys;

namespace WpfApplication1
{
    public enum Stat { NepessegEloszlas = 0, Heterogenitas, FerfiAg, NoiAg, SzarmazasiArany, Tipisys }
    public class NotifyUIBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OPC([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class ViewModel : NotifyUIBase
    {
        public DAL_Kezelo Database { get; set; }
        public Kolonia Kivalasztott { get; set; }
        public Statisys Statisztika { get; set; }
        private int size = 8;
        public int Size { get { return size; } set { size = value; } }
        public int ActualN { get; set; }
        public List<MintaEmber> MintaEmberLista { get; set; }
        private ObservableCollection<MintaEmber> mintaEmberLista_DB;
        public ObservableCollection<MintaEmber> MintaEmberLista_DB { get { return mintaEmberLista_DB; } set { mintaEmberLista_DB = value; OPC(); } }
        public Tipisys T { get; set; }

        Játéktér map;
        public Játéktér Map { get { return map; } set { map = value; OPC(); } }

        WriteableBitmap bitmap;
        public WriteableBitmap Bitmap
        {
            get { return bitmap; }
            set { bitmap = value; OPC(); }
        }

        private ViewModel()
        {
            Map = new Játéktér(new IOMap()._ReadToBoolArray("mitxcf.bmp", 127));
            Statisztika = new Statisys(Map);
            Bitmap = new WriteableBitmap(360, 360, 96d, 96d, PixelFormats.Bgr24, null);
            TerkepFrissites(Stat.NepessegEloszlas, null);
            Database = DAL_Kezelo_Singleton._GetInstance();
        }

        private static ViewModel _peldany;

        public static ViewModel Get()
        {
            if (_peldany == null)
                _peldany = new ViewModel();
            return _peldany;
        }

        public void TerkepFrissites(Stat stat, string cimke)
        {
            byte colR;
            byte colG;
            byte colB;
            Bitmap.Lock();

            int[,] szin_aranyos;
            switch ((int)stat)
            {
                case 0: szin_aranyos = Statisztika.NepessegEloszlas(); break;
                case 1: szin_aranyos = Statisztika.Heterogenitas_to_Map(); break;
                case 2: szin_aranyos = Statisztika._FerfiAgiLeszarmazottakAranya_to_Map(cimke); break;
                case 3: szin_aranyos = Statisztika._NoiAgiLeszarmazottakAranya_to_Map(cimke); break;
                case 4: szin_aranyos = Statisztika._SzarmazasiArany_to_Map(cimke); break;
                case 5: szin_aranyos = Statisztika.Tipizáció(T.GetElterjedes(int.Parse(cimke))); break;
                default: szin_aranyos = Statisztika.NepessegEloszlas(); break;
            }

            for (int x = 0; x < Map._Koloniak.GetLength(0); x++)
            {
                for (int y = 0; y < Map._Koloniak.GetLength(1); y++)
                {
                    if (Map._Koloniak[x, y] != null)
                    {
                        if (Map._Koloniak[x, y]._Lakossag == 0)
                        {
                            colR = 4;
                            colG = 206;
                            colB = 39;
                        }
                        else
                        {
                            colR = 255;
                            colG = (byte)(255 - (byte)szin_aranyos[x, y]);
                            colB = (byte)(255 - (byte)szin_aranyos[x, y]);
                        }
                    }
                    else
                    {
                        colR = 0;
                        colG = 0;
                        colB = 0;
                    }

                    DrawRectangle(Bitmap, Size * y, Size * x, Size, Size, Color.FromRgb(colR, colG, colB));
                }
            }

            Bitmap.Unlock();
        }

        private void DrawRectangle(WriteableBitmap writeableBitmap, int left, int top, int width, int height, Color color)
        {
            // Compute the pixel's color
            int colorData = color.R << 16; // R
            colorData |= color.G << 8; // G
            colorData |= color.B << 0; // B
            int bpp = writeableBitmap.Format.BitsPerPixel / 8;

            unsafe
            {
                for (int y = 0; y < height; y++)
                {
                    // Get a pointer to the back buffer
                    int pBackBuffer = (int)writeableBitmap.BackBuffer;

                    // Find the address of the pixel to draw
                    pBackBuffer += (top + y) * writeableBitmap.BackBufferStride;
                    pBackBuffer += left * bpp;

                    for (int x = 0; x < width; x++)
                    {
                        // Assign the color data to the pixel
                        *((int*)pBackBuffer) = colorData;

                        // Increment the address of the pixel to draw
                        pBackBuffer += bpp;
                    }
                }
            }

            writeableBitmap.AddDirtyRect(new Int32Rect(left, top, width, height));
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel VM;
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            VM = ViewModel.Get();
            DataContext = VM;

            VM.MintaEmberLista = new List<MintaEmber>();
            string[] filenames = Directory.GetFiles("mintaarcok");
            foreach (var item in filenames)
            {
                VM.MintaEmberLista.Add(new MintaEmber(item, true));
            }
            lstBox.ItemsSource = VM.MintaEmberLista;

            //Adatbázis
            VM.MintaEmberLista_DB = new ObservableCollection<MintaEmber>();
            var cimkek_DB = VM.Database._CimkeListaToStringLista;
            foreach (var item in cimkek_DB)
            {
                VM.MintaEmberLista.Add(new MintaEmber(item, true));
            }
            lstBox_database.ItemsSource = VM.MintaEmberLista_DB;
        }

        private void load_btn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = ".bmp képfájl|*.bmp";
            ofd.Title = "Válasszon egy .bmp kiterjesztésű képfájlt!";
            if (ofd.ShowDialog() == true)
            {
                string filename = ofd.FileName;
                VM.Map = new Játéktér(new IOMap()._ReadToBoolArray(filename, 127));
                VM.Statisztika = new Statisys(VM.Map);

                int x = VM.Map._Koloniak.GetLength(0);
                int y = VM.Map._Koloniak.GetLength(1);
                int size;
                if (x >= y)
                {
                    size = 360 / x;
                }
                else
                {
                    size = 360 / y;
                }
                if (size == 0)
                    size = 1;
                VM.Size = size;

                VM.Bitmap = new WriteableBitmap(VM.Size * x, VM.Size * y, 96d, 96d, PixelFormats.Bgr24, null);
                VM.TerkepFrissites(Stat.NepessegEloszlas, null);
            }
        }

        private void image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point Coord = e.GetPosition(image);

            List<Ember> LE = new List<Ember>();
            List<IEmberAdat> g = new List<IEmberAdat>();
            IO_Ember ioe = new IO_Ember();
            foreach (var item in VM.MintaEmberLista)
            {
                if (item.Checked == true)
                {
                    g.Add(ioe._Read(item.Filename)[0]);
                }
            }

            foreach (var item in VM.MintaEmberLista_DB)
            {
                if (item.Checked == true)
                {
                    Ember ember_DB = VM.Database._EmberElementAt(item.Index);
                    LE.Add(ember_DB);
                }
            }

            for (int i = 0; i < g.Count; i++)
            {
                LE.Add(new Ember(g[i]));
            }

            VM.Map._Elhelyez(LE, (int)((Coord.Y - 1) / VM.Size), (int)((Coord.X - 1) / VM.Size));
            VM.TerkepFrissites(Stat.NepessegEloszlas, null);
        }

        private async void simulation_btn_Click(object sender, RoutedEventArgs e)
        {
            if (VM.Map._Full_Lakossag == 0) return;

            VM.ActualN = SimulationParameters.N;
            SimulationParameters.N = 0;
            int i = 0;
            while (i < VM.ActualN)
            {
                i++;
                SimulationParameters.N++;
                VM.Map._NextStatys();
                await Task.Delay(300);
                VM.TerkepFrissites(Stat.NepessegEloszlas, null);

                if (VM.Map._Full_Lakossag == 0)
                {
                    MessageBox.Show("A " + i + ". generációnál kihalt az összes egyed.");
                    i = VM.ActualN;
                    SimulationParameters.N = VM.ActualN;
                }
            }
        }

        private void reset_btn_Click(object sender, RoutedEventArgs e)
        {
            VM.Map = new Játéktér(new IOMap()._ReadToBoolArray("mitxcf.bmp", 127));
            VM.Statisztika = new Statisys(VM.Map);
            VM.TerkepFrissites(Stat.NepessegEloszlas, null);
        }

        private void globalStatWindow_btn_Click(object sender, RoutedEventArgs e)
        {
            if (VM.Map._Full_Lakossag == 0)
                return;
            GlobalStatWindow gsw = new GlobalStatWindow();
            gsw.ShowDialog();
            VM.TerkepFrissites(Stat.NepessegEloszlas, null);
        }

        private void klaszterWindow_btn_Click(object sender, RoutedEventArgs e)
        {
            bool isGenderChk = (bool)gender_chk.IsChecked;
            if (VM.Map._Full_Lakossag == 0)
                return;
            KlaszterWindow kw = new KlaszterWindow(isGenderChk);
            kw.ShowDialog();
            VM.TerkepFrissites(Stat.NepessegEloszlas, null);
        }
        private void selectAll_btn_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in VM.MintaEmberLista)
            {
                item.Checked = true;
            }
        }

        private void deselectAll_btn_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in VM.MintaEmberLista)
            {
                item.Checked = false;
            }
        }

        private void selectAll_DB_btn_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in VM.MintaEmberLista_DB)
            {
                item.Checked = true;
            }
        }

        private void deselectAll_DB_btn_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in VM.MintaEmberLista_DB)
            {
                item.Checked = false;
            }
        }

        private void rnd_btn_Click(object sender, RoutedEventArgs e)
        {
            string szoveg = szazalek.Text;
            int esely;
            bool isNumber = int.TryParse(szoveg, out esely);

            if (isNumber && esely > 0 && esely <= 100)
            {
                foreach (var item in VM.MintaEmberLista)
                {
                    item.Checked = false;
                }

                foreach (var item in VM.MintaEmberLista_DB)
                {
                    item.Checked = false;
                }

                foreach (var item in VM.MintaEmberLista)
                {
                    int rnd = Rnd._Next(1, 101);
                    if (rnd <= esely)
                        item.Checked = true;
                }

                foreach (var item in VM.MintaEmberLista_DB)
                {
                    int rnd = Rnd._Next(1, 101);
                    if (rnd <= esely)
                        item.Checked = true;
                }
            }
        }

        private void Database_removeAt_btn_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < VM.MintaEmberLista_DB.Count; i++)
            {
                if (VM.MintaEmberLista_DB[i].Checked)
                {
                    VM.Database._DeleteEmber(VM.MintaEmberLista_DB[i].Index);
                    for (int j = i + 1; j < VM.MintaEmberLista_DB.Count; j++)
                    {
                        VM.MintaEmberLista_DB[j].Index--;
                    }
                    VM.MintaEmberLista_DB.Remove(VM.MintaEmberLista_DB[i]);
                }
            }
        }
    }
}
