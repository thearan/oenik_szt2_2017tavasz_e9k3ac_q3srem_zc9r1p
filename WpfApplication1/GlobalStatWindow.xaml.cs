﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for GlobalStatWindow.xaml
    /// </summary>
    public partial class GlobalStatWindow : Window
    {
        private ViewModel VM;

        public GlobalStatWindow()
        {
            InitializeComponent();
            Loaded += GlobalStatWindow_Loaded;
        }

        private void GlobalStatWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.VM = ViewModel.Get();
            this.DataContext = VM;
            List<string> ffi = new List<string>();
            List<string> noi = new List<string>();

            for (int x = 0; x < VM.Map._Koloniak.GetLength(0); x++)
            {
                for (int y = 0; y < VM.Map._Koloniak.GetLength(1); y++)
                {
                    if (VM.Map._Koloniak[x, y] != null)
                    {
                        var ffi_seged = VM.Map._Koloniak[x, y]._Egyedek.Where(g => (g._Nem == true && !ffi.Contains(g._VegsoOs))).GroupBy(g => g._VegsoOs).ToDictionary(g => g.Key);
                        ffi.AddRange(ffi_seged.Keys);
                        var noi_seged = VM.Map._Koloniak[x, y]._Egyedek.Where(g => g._Nem == false && !noi.Contains(g._VegsoOs)).GroupBy(g => g._VegsoOs).ToDictionary(g => g.Key);
                        noi.AddRange(noi_seged.Keys);
                    }
                }
            }
            var all = ffi.Union(noi);
            lstAllBox.ItemsSource = all;
            lstMaleBox.ItemsSource = ffi;
            lstFemaleBox.ItemsSource = noi;
        }

        private void image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Point Coord = e.GetPosition(image);
            if (VM.Map._Koloniak[(int)((Coord.Y - 1) / VM.Size), (int)((Coord.X - 1) / VM.Size)] != null
                && VM.Map._Koloniak[(int)((Coord.Y - 1) / VM.Size), (int)((Coord.X - 1) / VM.Size)]._Lakossag != 0)
            {
                KoloniaStatWindow ksw = new KoloniaStatWindow();
                VM.Kivalasztott = VM.Map._Koloniak[(int)((Coord.Y - 1) / VM.Size), (int)((Coord.X - 1) / VM.Size)];
                ksw.ShowDialog();
                VM.Kivalasztott = null;
            }
        }

        private void nepsuruseg_radbtn_Click(object sender, RoutedEventArgs e)
        {
            VM.TerkepFrissites(Stat.NepessegEloszlas, null);
        }

        private void heterogenitas_radbtn_Click(object sender, RoutedEventArgs e)
        {
            VM.TerkepFrissites(Stat.Heterogenitas, null);
        }

        private void all_checked_Click(object sender, RoutedEventArgs e)
        {
            if((bool)((RadioButton)sender).IsChecked)
            {
                RadioButton rb = (RadioButton)sender;
                string cimke = (string)rb.Content;
                VM.TerkepFrissites(Stat.SzarmazasiArany, cimke);
            }
        }

        private void male_checked_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)((RadioButton)sender).IsChecked)
            {
                RadioButton rb = (RadioButton)sender;
                string cimke = (string)rb.Content;
                VM.TerkepFrissites(Stat.FerfiAg, cimke);
            }
        }

        private void female_checked_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)((RadioButton)sender).IsChecked)
            {
                RadioButton rb = (RadioButton)sender;
                string cimke = (string)rb.Content;
                VM.TerkepFrissites(Stat.NoiAg, cimke);
            }
        }


    }
}
