﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for KlaszterWindow.xaml
    /// </summary>
    public partial class KlaszterWindow : Window
    {
        ViewModel VM;
        private bool isGenderChk;
        private List<string> klaszterek;
        public KlaszterWindow(bool isGenderChk)
        {
            this.isGenderChk = isGenderChk;
            klaszterek = new List<string>();
            VM = ViewModel.Get();
            DataContext = VM;
            InitializeComponent();
            Loaded += KlaszterWindow_Loaded;
            Closing += KlaszterWindow_Closing;
        }

        private void KlaszterWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            for (int x = 0; x < VM.Map._Koloniak.GetLength(0); x++)
            {
                for (int y = 0; y < VM.Map._Koloniak.GetLength(1); y++)
                {
                    if (VM.Map._Koloniak[x, y] != null)
                    {
                        foreach (var item in VM.Map._Koloniak[x, y]._Egyedek)
                        {
                            item._HozzajukLegkozelebbEnVagyok = new List<int>();
                            item._HozzamVanLegkozelebb = 0;
                        }
                    }
                }
            }
            VM.T = null;
        }

        private void KlaszterWindow_Loaded(object sender, RoutedEventArgs e)
        {
            VM.T = new Tipisys(VM.Map);
            if (isGenderChk)
            {
                VM.T.KlaszterezesInditas(Tipisys.TavSzamMode.EgyenesagiHaplocsoportokBeleszamitasaval);
            }
            else
            {
                VM.T.KlaszterezesInditas(Tipisys.TavSzamMode.EgyenesagiHaplocsoportokBeleszamitasaNelkul);
            }
            for (int i = 0; i < VM.T.KlaszterekSzama; i++)
            {
                klaszterek.Add(VM.T.GetKlaszterToString(i));
            }

            lstKlaszterek.ItemsSource = klaszterek;
        }

        private void ffi_atlagarc_Click(object sender, RoutedEventArgs e)
        {
            if (lstKlaszterek.SelectedItem == null) return;
            Ember arc = VM.T.GetAtlag(lstKlaszterek.SelectedIndex, true);

            if (File.Exists("klaszterArc_ffi" + ".fg"))
            {
                File.Delete("klaszterArc_ffi" + ".fg");
            }

            IO_Ember ioe = new IO_Ember();
            ioe._Write(arc._Gének, "klaszterArc_ffi");

            Process p = Process.Start(@"FaceGen Modeller 3.1\facegen.exe");
            Thread.Sleep(2000);

            SendKeys.SendWait("{enter}");
            SendKeys.SendWait("^(o)");
            Thread.Sleep(500);

            SendKeys.SendWait(Directory.GetCurrentDirectory() + "\\klaszterArc_ffi.fg");
            SendKeys.SendWait("{enter}");
        }

        private void noi_atlagarc_Click(object sender, RoutedEventArgs e)
        {
            if (lstKlaszterek.SelectedItem == null) return;
            Ember arc = VM.T.GetAtlag(lstKlaszterek.SelectedIndex, false);
            if (File.Exists("klaszterArc_noi" + ".fg"))
            {
                File.Delete("klaszterArc_noi" + ".fg");
            }

            IO_Ember ioe = new IO_Ember();
            ioe._Write(arc._Gének, "klaszterArc_noi");

            Process p = Process.Start(@"FaceGen Modeller 3.1\facegen.exe");
            Thread.Sleep(2000);

            SendKeys.SendWait("{enter}");
            SendKeys.SendWait("^(o)");
            Thread.Sleep(500);

            SendKeys.SendWait(Directory.GetCurrentDirectory() + "\\klaszterArc_noi.fg");
            SendKeys.SendWait("{enter}");
        }

        private void selected_klaszter_Changed(object sender, SelectionChangedEventArgs e)
        {
            if (lstKlaszterek.SelectedItem == null)
            {
                VM.TerkepFrissites(Stat.NepessegEloszlas, null);
                return;
            }
            VM.TerkepFrissites(Stat.Tipisys, lstKlaszterek.SelectedIndex.ToString());
        }
    }
}
