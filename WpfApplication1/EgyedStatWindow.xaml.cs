﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    public class BoolToValueConverter<T> : IValueConverter
    {
        public T FalseValue { get; set; }
        public T TrueValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return FalseValue;
            else
                return (bool)value ? TrueValue : FalseValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value != null ? value.Equals(TrueValue) : false;
        }
    }

    public class BoolToStringConverter : BoolToValueConverter<string> { }

    /// <summary>
    /// Interaction logic for EgyedStatWindow.xaml
    /// </summary>
    public partial class EgyedStatWindow : Window
    {
        private Ember egyed;
        private ViewModel VM;
        private bool kimentve = false;
        public EgyedStatWindow(Ember egyed)
        {
            InitializeComponent();
            VM = ViewModel.Get();
            this.egyed = egyed;
            this.DataContext = egyed;

            Loaded += EgyedStatWindow_Loaded;
        }

        private void EgyedStatWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var szarmazasi_arany = egyed._Gének.GroupBy(g => g._Cimke).ToDictionary(g => g.Key, g => Math.Round((g.Count() / (double)egyed._Gének.Count()) * 100, 2));
            lstArany.ItemsSource = szarmazasi_arany;
        }

        private void egyed_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists("egyed" + ".fg"))
            {
                File.Delete("egyed" + ".fg");
            }

            IO_Ember ioe = new IO_Ember();
            ioe._Write(egyed._Gének, "egyed");

            Process p = Process.Start(@"FaceGen Modeller 3.1\facegen.exe");
            Thread.Sleep(2000);

            SendKeys.SendWait("{enter}");
            SendKeys.SendWait("^(o)");
            Thread.Sleep(500);

            SendKeys.SendWait(Directory.GetCurrentDirectory() + "\\egyed.fg");
            SendKeys.SendWait("{enter}");
        }

        private void saveToDatabase_btn_Click(object sender, RoutedEventArgs e)
        {
            if (!kimentve)
            {
                VM.Database._AddEmber(egyed);
                VM.MintaEmberLista_DB.Add(new MintaEmber(egyed._VegsoOs, true, VM.Database.EmberCounter));
                kimentve = true;
            }
        }
    }
}
