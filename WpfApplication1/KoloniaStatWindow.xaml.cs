﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for KoloniaStatWindow.xaml
    /// </summary>
    public partial class KoloniaStatWindow : Window
    {
        ViewModel VM;
        public KoloniaStatWindow()
        {
            InitializeComponent();
            Loaded += KoloniaStatWindow_Loaded;
        }

        private void KoloniaStatWindow_Loaded(object sender, RoutedEventArgs e)
        {
            VM = ViewModel.Get();
            DataContext = VM;
            lstKolonia.ItemsSource = VM.Kivalasztott._Egyedek;

            var vegso_Os_Count = VM.Kivalasztott._Egyedek.GroupBy(x => x._VegsoOs).ToDictionary(g => g.Key, g => Math.Round((g.Count() / (double)VM.Kivalasztott._Egyedek.Count) * 100, 2));
            lstArany.ItemsSource = vegso_Os_Count;
        }

        private void ferfi_atlagarc_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists("ffi_atlagarc" + ".fg"))
            {
                File.Delete("ffi_atlagarc" + ".fg");
            }

            var ffi_atlagarc = StatisysExtensions._AtlagArc_Ferfi(VM.Kivalasztott);
            IO_Ember ioe = new IO_Ember();
            ioe._Write(ffi_atlagarc._Gének, "ffi_atlagarc");

            Process p = Process.Start(@"FaceGen Modeller 3.1\facegen.exe");
            Thread.Sleep(2000);

            SendKeys.SendWait("{enter}");
            SendKeys.SendWait("^(o)");
            Thread.Sleep(500);

            SendKeys.SendWait(Directory.GetCurrentDirectory() + "\\ffi_atlagarc.fg");
            SendKeys.SendWait("{enter}");
        }

        private void noi_atlagarc_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists("noi_atlagarc" + ".fg"))
            {
                File.Delete("noi_atlagarc" + ".fg");
            }

            var noi_atlagarc = StatisysExtensions._AtlagArc_No(VM.Kivalasztott);
            IO_Ember ioe = new IO_Ember();
            ioe._Write(noi_atlagarc._Gének, "noi_atlagarc");

            Process p = Process.Start(@"FaceGen Modeller 3.1\facegen.exe");
            Thread.Sleep(2000);

            SendKeys.SendWait("{enter}");
            SendKeys.SendWait("^(o)");
            Thread.Sleep(500);

            SendKeys.SendWait(Directory.GetCurrentDirectory() + "\\noi_atlagarc.fg");
            SendKeys.SendWait("{enter}");

        }

        private void egyed_stat_Click(object sender, RoutedEventArgs e)
        {
            if (lstKolonia.SelectedItem == null) return;
            Ember egyed = VM.Kivalasztott._Egyedek[lstKolonia.SelectedIndex];
            EgyedStatWindow esw = new EgyedStatWindow(egyed);
            esw.ShowDialog();
        }
    }
}
