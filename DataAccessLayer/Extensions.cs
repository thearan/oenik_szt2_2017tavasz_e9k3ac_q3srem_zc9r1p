﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public static class Extensions
    {
        public static bool EqualTo(this IEMBERADAT ea, IEMBERADAT ea2)
        {
            bool output = true;

            output = output &&
                (ea.THE_CIMKE == ea2.THE_CIMKE) &&
                (ea.NEM.Value == ea2.NEM.Value) &&
                (ea.GENE.Count == ea2.GENE.Count);
            for (int i  = 0; i  < ea.GENE.Count; i ++)
            {
                output = output && (ea.GENE.ElementAt(i).THE_NUMBER.Value == ea2.GENE.ElementAt(i).THE_NUMBER.Value);
                output = output && (ea.GENE.ElementAt(i).THE_CIMKE == ea2.GENE.ElementAt(i).THE_CIMKE);
            }

            return output;
            
        }
    }
}
