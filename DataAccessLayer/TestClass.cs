﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    [TestFixture]
    public class TestClass
    {
        [Test]
        public void RepoTest()
        {
            #region arrange
            DbContext dbc = new GenebaseEntities();
            ICimkeRepo cimkerepo = new CimkeRepo(dbc);
            IGeneRepo generepo = new GeneRepo(dbc);
            IEmberAdatRepo emberadatrepo = new EmberAdatRepo(dbc);

            #endregion


            #region act
            cimkerepo.Insert(new CIMKE() { NAME = "proba" });
            CIMKE temp = new CIMKE() { NAME = "proba2" };
            cimkerepo.Insert(temp);
            cimkerepo.Delete(temp);
            var q = cimkerepo.GetAll().Single().NAME;
            var q1 = cimkerepo.GetAll();



            emberadatrepo.Insert(new EMBERADAT() { THE_CIMKE = "proba", id = 0, NEM = false });
            emberadatrepo.ModifyCimke(0, "proba3");
            emberadatrepo.ModifyCimke(0, "proba4");
            emberadatrepo.ModifyNem(0, true);
            emberadatrepo.ModifyNem(0, true);
            emberadatrepo.Insert(new EMBERADAT() { THE_CIMKE = "proba", id = 1, NEM = true });
            emberadatrepo.Insert(new EMBERADAT() { THE_CIMKE = "proba5", id = 6, NEM = false });
            emberadatrepo.Delete(emberadatrepo.GetAll().Where(x => x.id == 6).Single());
            var q2 = emberadatrepo.GetAll();


            generepo.Insert(new GENE() { EMBER_ID = 0, GENE_ID = 32, THE_CIMKE = "probacimke43", THE_NUMBER = 666 });
            generepo.Insert(new GENE() { EMBER_ID = 0, GENE_ID = 33, THE_CIMKE = "probacimke44", THE_NUMBER = 691 });
            generepo.Insert(new GENE() { EMBER_ID = 1, GENE_ID = 3, THE_CIMKE = "proba", THE_NUMBER = 667 });
            generepo.Insert(new GENE() { EMBER_ID = 1, GENE_ID = 4, THE_CIMKE = "proba4", THE_NUMBER = 667 });
            generepo.Delete(generepo.GetAll().Where(x => x.EMBER_ID == 0 && x.GENE_ID == 32).Single());
            generepo.Delete(generepo.GetAll().Where(x => x.EMBER_ID == 0 && x.GENE_ID == 33).Single());
            generepo.ModifyThe_Cimke(1, 3, "proba");
            generepo.ModifyThe_Cimke(1, 4, "proba");
            generepo.ModifyThe_Number(1, 3, 1024);
            generepo.ModifyThe_Number(1, 4, 256);
            var q3 = generepo.GetAll();
            #endregion


            #region assert
            Assert.That(q, Is.EqualTo("proba"));
            Assert.That(q1.Count(), Is.EqualTo(6));
            Assert.That(q2.Count(), Is.EqualTo(2));
            Assert.That(q3.Count(), Is.EqualTo(2));
            #endregion

        }
        [Test]
        public void QueriesTest()
        {
            #region arrange
            DbContext dbc = new GenebaseEntities();
            CimkeRepo cimkerepo = new CimkeRepo(dbc);
            GeneRepo generepo = new GeneRepo(dbc);
            EmberAdatRepo emberadatrepo = new EmberAdatRepo(dbc);
            Queries qs = new Queries(cimkerepo, generepo, emberadatrepo);
            #endregion

            #region act
            cimkerepo.Insert(new CIMKE() { NAME = "proba" });
            cimkerepo.Insert(new CIMKE() { NAME = "proba2" });
            emberadatrepo.Insert(new EMBERADAT() { THE_CIMKE = "proba", id = 0, NEM = false });
            emberadatrepo.Insert(new EMBERADAT() { THE_CIMKE = "proba2", id = 32, NEM = false });
            var q1 = qs.CimkeListaToStringLista();
            var q2 = qs.GetEmberAdatByID(32);
            var q3 = qs.GetAllEMBERADAT();
            var q4 = qs.GetGenesOfEmber(qs.GetEmberAdatByID(32));
            #endregion

            #region Assert
            Assert.That(q1.ElementAt(0), Is.EqualTo("proba"));
            Assert.That(q1.ElementAt(1), Is.EqualTo("proba2"));
            Assert.That(q1.Count(), Is.EqualTo(2));
            Assert.That(q2.id, Is.EqualTo(32));
            Assert.That(q2.THE_CIMKE, Is.EqualTo("proba2"));
            Assert.That(q2.NEM, Is.EqualTo(false));
            Assert.That(q3.Count, Is.EqualTo(2));
            #endregion
        }

        [Test]
        public void EMBERADATEXTENSIONTest()
        {
            #region arrange
            DbContext dbc = new GenebaseEntities();
            CimkeRepo cimkerepo = new CimkeRepo(dbc);
            GeneRepo generepo = new GeneRepo(dbc);
            EmberAdatRepo emberadatrepo = new EmberAdatRepo(dbc);
            Random R = new Random();
            decimal[] g = new decimal[144];
            for (int i = 0; i < g.Length; i++)
            {
                g[i] = R.Next(20000, 50000);
            }
            #endregion

            #region act
            EMBERADAT ea1 = new EMBERADAT() { id = 0, NEM = false, THE_CIMKE = "proba" };
            emberadatrepo.Insert(ea1);
            EMBERADAT ea2 = new EMBERADAT() { id = 1, NEM = false, THE_CIMKE = "proba" };
            emberadatrepo.Insert(ea2);

            for (int i = 0; i < g.Length; i++)
            {
                generepo.Insert(new GENE() { EMBER_ID = 0, GENE_ID = i, THE_CIMKE = i.ToString(), THE_NUMBER = g[i] });
                generepo.Insert(new GENE() { EMBER_ID = 1, GENE_ID = i, THE_CIMKE = i.ToString(), THE_NUMBER = g[i] });
            }

            List<EMBERADAT> testlist = emberadatrepo.GetAll().ToList();
            EMBERADAT ea1b = testlist[0];
            EMBERADAT ea2b = testlist[1];
            EMBERADAT eaother = new EMBERADAT() { THE_CIMKE = "gfdrtz", id = 6, NEM = true };

            bool test1 = ea1.EqualTo(ea1b) && ea1b.EqualTo(ea1); 
            bool test2 = ea2.EqualTo(ea2b) && ea2b.EqualTo(ea2);
            bool test3 = ea1.EqualTo(ea2) && ea2.EqualTo(ea1);
            bool test4 = ea1b.EqualTo(ea2b) && ea2b.EqualTo(ea1b);

            bool test5 = ea1.EqualTo(eaother) && eaother.EqualTo(ea1);
            bool test6 = ea2.EqualTo(eaother) && eaother.EqualTo(ea2);
            bool test7 = ea1b.EqualTo(eaother) && eaother.EqualTo(ea1b);
            bool test8 = ea2b.EqualTo(eaother) && eaother.EqualTo(ea2b);
            #endregion

            #region assert
            Assert.That(test1, Is.EqualTo(true));
            Assert.That(test2, Is.EqualTo(true));
            Assert.That(test3, Is.EqualTo(true));
            Assert.That(test4, Is.EqualTo(true));
            Assert.That(test5, Is.EqualTo(false));
            Assert.That(test6, Is.EqualTo(false));
            Assert.That(test7, Is.EqualTo(false));
            Assert.That(test8, Is.EqualTo(false));
            #endregion

        }
    }
}
