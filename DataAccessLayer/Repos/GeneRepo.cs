﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class GeneRepo : Repo<GENE>, IGeneRepo
    {
        public GeneRepo(DbContext entities) : base(entities)
        {
        }

        public new void Insert(GENE newEntity)
        {
            if (entities.Set<CIMKE>().Where(x => (x.NAME == newEntity.THE_CIMKE)).Count() == 0)
            {
                entities.Set<CIMKE>().Add(new CIMKE() { NAME = newEntity.THE_CIMKE });
            }
            base.Insert(newEntity);
        }

        public void ModifyThe_Number(decimal emberid, decimal idx, Nullable<decimal> newValue)
        {
            entities.Set<GENE>().Single(x => x.EMBER_ID == emberid && x.GENE_ID == idx).THE_NUMBER = newValue;
            entities.SaveChanges();
        }

        public void ModifyThe_Cimke(decimal emberid, decimal idx, string newCimke)
        {
            entities.Set<GENE>().Single(x => x.EMBER_ID == emberid && x.GENE_ID == idx).THE_CIMKE  = newCimke;
            entities.SaveChanges();
        }

        public void InsertOrReplace(decimal emberid, decimal idx, string newCimke, Nullable<decimal> newValue)
        {
            GENE g = entities.Set<GENE>().SingleOrDefault(x => x.EMBER_ID == emberid && x.GENE_ID == idx);
            if (g == default(GENE))
            {
                g = new GENE();
                g.EMBER_ID = emberid;
                g.GENE_ID = idx;
                g.THE_CIMKE = newCimke;
                g.THE_NUMBER = newValue;
                Insert(g);
            }
            else
            {
                g.THE_CIMKE = newCimke;
                g.THE_NUMBER = newValue;
            }
            entities.SaveChanges();
        }

    }
}
