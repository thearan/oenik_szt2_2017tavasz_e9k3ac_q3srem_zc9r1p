﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class CimkeRepo : Repo<CIMKE>, ICimkeRepo
    {
        public CimkeRepo(DbContext entities) : base(entities)
        {
        }

        public new void Insert(CIMKE newEntity)
        {
            base.Insert(newEntity);
        }
    }
}
