﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class EmberAdatRepo : Repo<EMBERADAT>, IEmberAdatRepo
    {
        public EmberAdatRepo(DbContext entities) : base(entities)
        {
        }

        public new void Insert(EMBERADAT newEntity)
        {
            if (entities.Set<CIMKE>().Where(x => (x.NAME == newEntity.THE_CIMKE)).Count() == 0)
            { entities.Set<CIMKE>().Add(new CIMKE() { NAME = newEntity.THE_CIMKE }); }
            base.Insert(newEntity);
        }

        public void ModifyCimke(decimal id, string newcimke)
        {
            if (entities.Set<CIMKE>().Where(x => (x.NAME == newcimke)).Count() == 0)
            { entities.Set<CIMKE>().Add(new CIMKE() { NAME = newcimke }); }

            entities.Set<EMBERADAT>().Single(x => x.id == id).THE_CIMKE = newcimke;
            entities.SaveChanges();
        }
        public void ModifyNem(decimal id, bool newnem)
        {
            entities.Set<EMBERADAT>().Single(x => x.id == id).NEM = newnem;
            entities.SaveChanges();
        }
    }
}
