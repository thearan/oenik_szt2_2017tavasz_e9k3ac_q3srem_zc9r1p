﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class Repo<TEntity> : IRepo<TEntity> where TEntity : class
    {
        protected DbContext entities;
        public Repo(DbContext entities)
        {
            this.entities = entities;
        }


        public void Delete(TEntity OldEntity)
        {
            entities.Set<TEntity>().Remove(OldEntity);
            entities.SaveChanges();
            
        }

        public IQueryable<TEntity> GetAll()
        {
            return entities.Set<TEntity>();
        }

        public void Insert(TEntity newEntity)
        {
            entities.Set<TEntity>().Add(newEntity);
            entities.SaveChanges();
        }

        public void DeleteAll()
        {
            entities.Set<TEntity>().RemoveRange(GetAll() as IEnumerable<TEntity>);
            entities.SaveChanges();
        }
    }
}
