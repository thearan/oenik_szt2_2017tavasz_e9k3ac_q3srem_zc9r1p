﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccessLayer
{
    public class DAL_EmberAdat
    {
        
        static decimal idcounter = -1;
        EMBERADAT emberadat;
        public decimal Id { get { return emberadat.id; } }
        EmberAdatRepo emberadatrepo = EmberAdatRepoFactory._Get();
        GeneRepo generepo = GeneRepoFactory._Get();
        static decimal[] nemiKromoszomaLokuszok = new decimal[] { 2, 3, 5, 7, 8, 9, 13, 80, 82, 83, 84, 86, 97, 111 };


        public DAL_EmberAdat(DAL_Gene[] genes, string cimke0, bool tartalmazhateujcimket)
        {
            idcounter++;
            if (tartalmazhateujcimket)
            {
                DataAccessLayer.GlobalCimkek._Add(cimke0);
            }
            //string temp = GlobalCimkek._CimkekToString.ElementAt(0);
            emberadat = new EMBERADAT() { id = idcounter , THE_CIMKE = cimke0};
            emberadatrepo.Insert(emberadat);
            for (int i = 0; i < genes.Length; i++)
            {
                genes[i]._Cimke = cimke0;
                genes[i] = new DAL_Gene(genes, idcounter, i);
                
            }


        }



        public bool nem
        {
            get { return emberadat.NEM.Value; }
            set { emberadatrepo.ModifyNem(emberadat.id, value); }
        }
        public IGene[] gének
        {
            get
            {
                return (from akt in emberadat.GENE
                        where akt.GENE_ID < 130
                        select new DAL_Gene
                            { _TheNumber = (ushort)akt.THE_NUMBER,
                            _Cimke = akt.THE_CIMKE }).ToArray();
            }
            set
            {
                for (int i = 0; i < 130; i++)
                {
                    generepo.InsertOrReplace(emberadat.id, i, value[i]._Cimke, value[i]._TheNumber);
                }
            }
        }
        public IGene[] nemikromoszoma
        {
            get
            {
                return (from akt in emberadat.GENE
                        where nemiKromoszomaLokuszok.Contains(akt.GENE_ID)
                        orderby akt.GENE_ID ascending
                        select new DAL_Gene
                        {
                            _TheNumber = (ushort)akt.THE_NUMBER,
                            _Cimke = akt.THE_CIMKE
                        }).ToArray();
            }
            set
            {
                for (int i = 0; i < nemiKromoszomaLokuszok.Length; i++)
                {
                    generepo.InsertOrReplace(emberadat.id, nemiKromoszomaLokuszok[i], value[i]._Cimke, value[i]._TheNumber);
                }
            }
        }
        public IGene[] ellentetesNemiKromoszoma
        {
            get
            {
                return (from akt in emberadat.GENE
                        where akt.GENE_ID >= 130
                        orderby akt.GENE_ID ascending
                        select new DAL_Gene
                        {
                            _TheNumber = (ushort)akt.THE_NUMBER,
                            _Cimke = akt.THE_CIMKE
                        }).ToArray();
            }
            set
            {
                for (int i = 0; i < value.Length; i++)
                {
                    generepo.InsertOrReplace(emberadat.id, (i + 130) , value[i]._Cimke, value[i]._TheNumber);
                }
            }
        }

        public string cimke
        {
            get { return emberadat.THE_CIMKE; }
            set { emberadatrepo.ModifyCimke(emberadat.id, value); }
        }
    }
}
