﻿using System.Collections.Generic;

namespace DataAccessLayer
{
    public interface IEMBERADAT
    {
        CIMKE CIMKE { get; set; }
        ICollection<GENE> GENE { get; set; }
        decimal id { get; set; }
        bool? NEM { get; set; }
        string THE_CIMKE { get; set; }

        //bool EqualTo(EMBERADAT ea);
    }
}