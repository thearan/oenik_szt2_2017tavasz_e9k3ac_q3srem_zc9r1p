﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public interface IEmberAdatRepo : IRepo<EMBERADAT>
    {
        void ModifyCimke(decimal id, string newcimke);
        void ModifyNem(decimal id, bool newnem);
        void DeleteAll();
    }
}