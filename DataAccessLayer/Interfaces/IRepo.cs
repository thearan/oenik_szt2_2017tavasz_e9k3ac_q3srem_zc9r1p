﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public interface IRepo<TEntity>
        where TEntity : class
    {
        void Insert(TEntity newEntity);
        void Delete(TEntity OldEntity);
        IQueryable<TEntity> GetAll();
    }
}
