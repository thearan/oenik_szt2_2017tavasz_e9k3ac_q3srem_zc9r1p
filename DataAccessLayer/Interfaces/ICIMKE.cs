﻿using System.Collections.Generic;

namespace DataAccessLayer
{
    public interface ICIMKE
    {
        ICollection<EMBERADAT> EMBERADAT { get; set; }
        ICollection<GENE> GENE { get; set; }
        string NAME { get; set; }
    }
}