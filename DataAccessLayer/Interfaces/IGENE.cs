﻿namespace DataAccessLayer
{
    public interface IGENE
    {
        CIMKE CIMKE { get; set; }
        EMBERADAT EMBERADAT { get; set; }
        decimal EMBER_ID { get; set; }
        decimal GENE_ID { get; set; }
        string THE_CIMKE { get; set; }
        decimal? THE_NUMBER { get; set; }
    }
}