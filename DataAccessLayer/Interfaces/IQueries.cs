﻿using System.Collections.Generic;

namespace DataAccessLayer
{
    public interface IQueries
    {
        IEnumerable<string> CimkeListaToStringLista();
        IEnumerable<IEMBERADAT> GetAllEMBERADAT();
        IEMBERADAT GetEmberAdatByID(int i);
        IEnumerable<IGENE> GetGenesOfEmber(IEMBERADAT ea);
    }
}