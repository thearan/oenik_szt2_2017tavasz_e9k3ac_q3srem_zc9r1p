﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public interface IGeneRepo : IRepo<GENE>
    {
        void ModifyThe_Cimke(decimal emberid, decimal idx, string newCimke);
        void ModifyThe_Number(decimal emberid, decimal idx, decimal? newValue);
        void InsertOrReplace(decimal emberid, decimal idx, string newCimke, Nullable<decimal> newValue);
        void DeleteAll();
    }
}