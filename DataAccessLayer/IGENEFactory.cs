﻿using System;

namespace DataAccessLayer
{
    public class IGENEFactory
    {
        public static GENE Create(int embercounter, int i, string _Cimke, ushort _TheNumber)
        {
            return new GENE()
            {
                EMBER_ID = embercounter,
                GENE_ID = i,
                THE_CIMKE = _Cimke,
                THE_NUMBER = _TheNumber
            };
        }

        public static GENE Create(string _Cimke, ushort _TheNumber)
        {
            return new GENE()
            {
                THE_CIMKE = _Cimke,
                THE_NUMBER = _TheNumber
            };
        }

        public static GENE Create(string _Cimke, ushort _TheNumber, int v)
        {
            return new GENE()
            {
                GENE_ID = v,
                THE_CIMKE = _Cimke,
                THE_NUMBER = _TheNumber
            };
        }
    }
}