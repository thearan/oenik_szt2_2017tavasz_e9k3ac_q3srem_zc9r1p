﻿using System;

namespace DataAccessLayer
{
    public class ICIMKEFactory
    {
        public static CIMKE Create(string cimke0)
        {
            return new CIMKE() { NAME = cimke0 };
        }
    }
}