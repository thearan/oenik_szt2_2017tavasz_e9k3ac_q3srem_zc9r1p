﻿using System;
using System.Data.Entity;

namespace DataAccessLayer
{
    public class IGeneRepoFactory
    {
        public static IGeneRepo Create(DbContext dbc)
        {
            return new GeneRepo(dbc);
        }
    }
}