﻿using System;
using System.Data.Entity;

namespace DataAccessLayer
{
    public class IEmberAdatRepoFactory
    {
        public static IEmberAdatRepo Create(DbContext dbc)
        {
            return new EmberAdatRepo(dbc);
        }
    }
}