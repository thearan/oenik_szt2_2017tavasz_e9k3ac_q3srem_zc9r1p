﻿namespace DataAccessLayer
{
    public class IQueriesFactory
    {
        public static IQueries Create(ICimkeRepo c, IGeneRepo g, IEmberAdatRepo e)
        {
            return new Queries(c as CimkeRepo, g as GeneRepo, e as EmberAdatRepo);
        }
    }
}