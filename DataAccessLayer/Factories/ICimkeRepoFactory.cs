﻿using System;
using System.Data.Entity;

namespace DataAccessLayer
{
    public class ICimkeRepoFactory
    {
        public static ICimkeRepo Create(DbContext dbc)
        {
            return new CimkeRepo(dbc);
        }
    }
}