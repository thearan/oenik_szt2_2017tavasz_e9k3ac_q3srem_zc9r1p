﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class Queries : IQueries
    {
        CimkeRepo cimkerepo;
        GeneRepo generepo;
        EmberAdatRepo emberadatrepo;

        public Queries(CimkeRepo cimkerepo, GeneRepo generepo, EmberAdatRepo emberadatrepo)
        {
            this.cimkerepo = cimkerepo;
            this.generepo = generepo;
            this.emberadatrepo = emberadatrepo;
        }

        public IEnumerable<string> CimkeListaToStringLista()
        {
                var q = cimkerepo.GetAll().Select(x => x.NAME);
                return q;
        }

        public IEMBERADAT GetEmberAdatByID(int i)
        {
            var q = from akt in emberadatrepo.GetAll()
                    where akt.id == i
                    select akt;
            return q.SingleOrDefault();
        }

        public IEnumerable<IGENE> GetGenesOfEmber(IEMBERADAT ea)
        {
            IEnumerable<GENE> genekfull = from akt in ea.GENE
                                           orderby akt.GENE_ID ascending
                                           select akt;
            return genekfull;
        }

        public IEnumerable<IEMBERADAT> GetAllEMBERADAT()
        {
           return emberadatrepo.GetAll();
        }
    }
}
