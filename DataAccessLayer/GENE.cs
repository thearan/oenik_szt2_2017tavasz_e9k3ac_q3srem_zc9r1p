//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class GENE : IGENE
    {
        public decimal EMBER_ID { get; set; }
        public decimal GENE_ID { get; set; }
        public Nullable<decimal> THE_NUMBER { get; set; }
        public string THE_CIMKE { get; set; }
    
        public virtual CIMKE CIMKE { get; set; }
        public virtual EMBERADAT EMBERADAT { get; set; }
    }
}
